package edu.rit.cs;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class KademliaNode extends UnicastRemoteObject implements Peer {
    static int RMI_PORT = 9095;
    static Node node = null;

    /**
     * @throws RemoteException Exception
     */
    protected KademliaNode() throws RemoteException {
    }

    /*
     * Start the RMI service
     */
    private static void startService() throws RemoteException {
        Registry registry;
        try {
            // Starting the RMI service
            registry = LocateRegistry.createRegistry(RMI_PORT);
            System.out.println("RMI registry created");
        } catch (RemoteException ex) {
            registry = LocateRegistry.getRegistry();
            System.out.println("RMI registry exists");
        }

        try {
            // Adding the objects to access
            Peer peer = new KademliaNode();
            registry.rebind("Node", peer);
            System.out.println("Registry Started");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Main function
     *
     * @param args Command line arguments
     * @throws RemoteException      Exception for remote objects
     * @throws UnknownHostException Exception for remote objects
     */
    public static void main(String[] args) throws RemoteException, UnknownHostException {
        startService();
        System.out.println("Service started");
        int nodeId = 0;
        try {
            nodeId = (args.length == 0)
                    ? Math.abs(InetAddress.getLocalHost().getHostAddress().hashCode()) % Node.TOTAL_NODES
                    : Integer.parseInt(args[0]);
            System.out.println("Node ID:" + nodeId);
        } catch (NumberFormatException ex) {
            System.out.println("Wrong ID format");
            System.exit(1);
        }
        node = new Node(nodeId);
    }


    /**
     * Inserts file into the network
     */
    @Override
    public int insert(File file) {
        if (file == null)
            return -1;
        return node.putFile(file);
    }


    /**
     * Finds the file in the network and returns it
     */
    @Override
    public File lookup(int hashCode) {
        if (hashCode < 0)
            return null;
        return node.getFile(hashCode);
    }
}
