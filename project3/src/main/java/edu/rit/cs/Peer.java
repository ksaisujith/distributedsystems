package edu.rit.cs;
import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Peer extends Remote {

    /**
     * Given a file, a peer will generate a hashCode for this file and place
     * it on a node according to the underlying DHT algorithm
     *
     * @param file File to insert inside the network
     * @return the hashCode of a file
     */
    int insert(File file) throws RemoteException;

    /**
     * Given the hashCode of a file, a peer should return the file.
     *
     * @param hashCode Hashcode of the file that has to be retrieved from the network
     * @return the file corresponding to the hashCode
     */
    File lookup(int hashCode) throws RemoteException;

}
