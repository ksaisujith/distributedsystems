package edu.rit.cs;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;

public class Client {
    static int RMI_PORT = 9095;

    /**
     * Main function of the client
     *
     * @param args Command line arguments
     * @throws UnknownHostException Exception for the host
     */
    public static void main(String[] args) throws UnknownHostException {
        int IP_value = Math.abs(InetAddress.getLocalHost().getHostAddress().hashCode()) % Node.TOTAL_NODES;
        String HOST = (IP_value ^ 2) < (IP_value ^ 4) ? "Node2" : "Node4";
        System.out.println("Connected to " + HOST);
        String url = "rmi://" + HOST + ":" + RMI_PORT + "/Node";
        new Client().start(url);
    }

    /**
     * Start of the options
     *
     * @param URL RMI connecting URL
     */
    public void start(String URL) {
        File newFile;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(System.in))) {
            Peer peer = (Peer) Naming.lookup(URL);
            int choice;
            while (true) {
                this.show_options();
                try {
                    choice = Integer.parseInt(in.readLine());
                    switch (choice) {
                        case 1:
                            System.out.println("Please enter the file along with its path");
                            newFile = new File(in.readLine());
                            System.out.println("Generated hashcode: " + peer.insert(newFile));
                            break;
                        case 2:
                            System.out.println("Please enter the hashcode of the file to retrieve");
                            newFile = peer.lookup(Integer.parseInt(in.readLine()));
                            if (newFile == null)
                                System.out.println("File doesn't exist in the network");
                            else {
                                System.out.println(newFile.toString());
                            }
                            break;
                        case 3:
                            System.exit(1);
                        default:
                            System.out.println("Wrong input provided");
                    }
                } catch (NumberFormatException n) {
                    System.out.println("Wrong input provided");
                }
            }
        } catch (IOException | NotBoundException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Options provided to client
     */
    public void show_options() {
        System.out.println("1. Insert file \n" +
                "2. Loook up File \n" +
                "3. Exit");
    }
}
