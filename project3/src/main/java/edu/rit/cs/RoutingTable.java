package edu.rit.cs;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class RoutingTable {
    final int nodeId;
    RoutingNode root = new RoutingNode();
    static NodeInfo myNode;
    final int UDP_PORT;
    int bits;

    /**
     * @param id   ID of this node
     * @param port PORT of the UDP
     * @param bits Number of bits in the network's maximum node
     */
    RoutingTable(int id, int port, int bits) {
        // Initiating the routing table
        this.nodeId = id;
        this.UDP_PORT = port;
        this.bits = bits;
        try {
            myNode = new NodeInfo(InetAddress.getLocalHost(), UDP_PORT, nodeId);
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Populates the table for this node
     */
    void populateTable() {
        RoutingNode temp_node = root;
        for (int index = bits - 1; index >= 0; index--) {
            if (((nodeId >> index) & 1) == 0) {
                temp_node.zero = new RoutingNode();
                temp_node = temp_node.zero;
            } else {
                temp_node.one = new RoutingNode();
                temp_node = temp_node.one;
            }
        }

        //Adding my nodeInfo to the table
        temp_node.node = myNode;
    }

    /**
     * Refreshes the routing table.
     * Searching the key and returning the nearest NodeInfo which might have the key
     *
     * @param search_from Search requested by
     * @param search_key  Key to be searched
     * @return Nearest node that might have the key
     */
    public NodeInfo findKey(NodeInfo search_from, int search_key) {
        //System.out.println("Searching "+ search_key + " searching from " + search_from);

        // ** Routing Table Refresh **
        NodeInfo prev_node = this.refreshTable(search_from);
        if (prev_node != null)
            return prev_node;

        // ** Key Search **
        RoutingNode temp_node = root;
        for (int index = bits - 1; index >= 0; index--) {
            int node_bit = (nodeId >> index) & 1;
            int search_bit = (search_key >> index) & 1;
            if ((search_bit ^ node_bit) == 0) {
                temp_node = (node_bit == 1) ? temp_node.one : temp_node.zero;
            } else
                break;
        }

        // if entry is blank then return current node
        // else if the node is not changed previously send the matched XXXX node
        // else send the notification to the closest node that the new node is alive
        //return temp_node.node == null ? myNode : (prev_node == null) ? temp_node.node : prev_node;
        return temp_node.node;
    }

    /**
     * Refreshing routing table by checking the node in the routing table and matching the prefix.
     * checks the node with the matched prefix node which is the nearest and updates if necessary
     *
     * @param node node to checks
     * @return Previous node if updated else null
     */
    public NodeInfo refreshTable(NodeInfo node) {
        // ** Routing Table Refresh **
        RoutingNode temp_node = root;
        NodeInfo returnNode = null;
        int refresh_nodeid = node.nodeId;
        for (int index = bits - 1; index >= 0; index--) {
            int node_bit = (nodeId >> index) & 1;
            int refresh_nodeid_bit = (refresh_nodeid >> index) & 1;
            if ((refresh_nodeid_bit ^ node_bit) == 0) {
                temp_node = (node_bit == 1) ? temp_node.one : temp_node.zero;
            } else {
                // Compare the closeness between the current NodeID and searchID to take action
                if (temp_node.node == null || ((temp_node.node.nodeId ^ nodeId) > (refresh_nodeid ^ nodeId))) {
                    returnNode = temp_node.node;
                    temp_node.node = node;
                }
                break;
            }
        }
        return returnNode;
    }

    /**
     * Returns the node for the key
     *
     * @param key Key ID
     * @return Node which might have the nearest node
     */
    NodeInfo getNodeInfo(int key) {
        RoutingNode temp = root;
        for (int index = bits - 1; index >= 0; index--) {
            int key_bit = (key >> index) & 1;
            int nodebit = (nodeId >> index) & 1;
            if ((key_bit ^ nodebit) == 0)
                temp = (key_bit == 1) ? temp.one : temp.zero;
            else
                break;
        }
        return temp.node;
    }

    /**
     * Returns all the nodes in the routing table
     *
     * @return List of nodes in the routing table
     */
    List<NodeInfo> getAllNodes() {
        List<NodeInfo> all_nodes = new ArrayList<>();
        RoutingNode temp = root;
        getAllNodesHelper(temp, all_nodes);
        return all_nodes;
    }

    /**
     * Helper function
     *
     * @param temp_node temporary node required for processing
     * @param list      List of all nodes
     */
    private void getAllNodesHelper(RoutingNode temp_node, List<NodeInfo> list) {
        if (temp_node == null)
            return;
        if (temp_node.node != null)
            list.add(temp_node.node);
        getAllNodesHelper(temp_node.zero, list);
        getAllNodesHelper(temp_node.one, list);
    }

    /**
     * Displays the routing table
     */
    void displayTable() {
        System.out.println(root);
    }

    public void remove(NodeInfo lookup_node) {
        int key = lookup_node.nodeId;
        RoutingNode temp = root;
        for (int index = bits - 1; index >= 0; index--) {
            int key_bit = (key >> index) & 1;
            int nodebit = (nodeId >> index) & 1;

            if ((key_bit ^ nodebit) == 0) {
                temp = (key_bit == 1) ? temp.one : temp.zero;
            } else {
                break;
            }
        }
        if (temp.node != null)
            System.out.println("REMOVED INACTIVE NODE : " + lookup_node.nodeId);
        temp.node = null;
    }
}

class NodeInfo implements Serializable {
    InetAddress ipAddress;
    int udpPort;
    int nodeId;

    /**
     * Node information node
     *
     * @param ip InetAddress of the node
     * @param port PORT through which the communication takes place
     * @param id ID of the node
     */
    NodeInfo(InetAddress ip, int port, int id) {
        this.ipAddress = ip;
        this.udpPort = port;
        this.nodeId = id;
    }

    @Override
    public String toString() {
        return "NodeInfo{" +
                "ipAddress='" + ipAddress + '\'' +
                ", udpPort=" + udpPort +
                ", nodeId=" + nodeId +
                '}';
    }
}

class RoutingNode {
    // Node of the routing table
    RoutingNode zero = null;
    RoutingNode one = null;
    NodeInfo node = null;

    @Override
    public String toString() {
        return "RoutingNode{" +
                "zero=" + zero +
                ", one=" + one +
                ", node=" + node +
                '}';
    }
}