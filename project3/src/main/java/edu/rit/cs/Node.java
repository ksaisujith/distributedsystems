package edu.rit.cs;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node extends Thread {
    static final int TOTAL_NODES = 16, RETRY = 3;
    static final boolean DEBUG_MODE = false;
    static RoutingTable routingTable;
    static final int LOOKUP_REQUEST_PORT = 9090, LOOKUP_RESPONSE_PORT = 9091, FILE_TRANSFER_PORT = 9092;
    static final int OTHER_OPS_REQUEST_PORT = 9093, OTHER_OPS_RESPONSE_PORT = 9094;
    static final int TIMEOUT = 4000;
    final int nodeID;
    static Map<Integer, File> storedFiles = new HashMap<>();
    private int threadType;

    // Thread handling params
    private NodeInfo searchFrom;
    private int searchKey;
    private int opsType;

    //static anchor nodes
    static NodeInfo anchornode1;
    static NodeInfo anchornode2;


    /**
     * Initial constructor
     *
     * @param id Unique identifier of this node
     */
    Node(int id) {
        this.nodeID = id;
        routingTable = new RoutingTable(this.nodeID, LOOKUP_REQUEST_PORT, (int) (Math.log(TOTAL_NODES) / Math.log(2)));

        routingTable.populateTable();

        // Lookup request listener thread
        new Node(id, 1).start();

        // Listener thread to transfer files
        new Node(id, 3).start();

        // Listener thread to listen others ops
        new Node(id, 4).start();

        // Booststrapping
        try {
            anchornode1 = new NodeInfo(InetAddress.getByName("Node2"), LOOKUP_REQUEST_PORT, 2);
            anchornode2 = new NodeInfo(InetAddress.getByName("Node4"), LOOKUP_REQUEST_PORT, 4);

            NodeInfo temp = anchornode1;
            // Self looking up with both the anchor nodes (Bootstrapping)
            while (temp.nodeId != this.nodeID) {
                try {
                    if (DEBUG_MODE)
                        System.out.println("Bootstrapping with" + temp.nodeId);
                    routingTable.refreshTable(temp);
                } catch (Exception ex) {
                    if (DEBUG_MODE)
                        ex.printStackTrace();
                    break;
                }
                temp = this.lookup(temp, this.nodeID);
            }

            // Self looking up from the anchor node (Bootstrapping)
            temp = anchornode2;
            while (temp.nodeId != this.nodeID) {
                if (DEBUG_MODE)
                    System.out.println("Bootstrapping with" + temp.nodeId);
                try {
                    routingTable.refreshTable(temp);
                } catch (Exception ex) {
                    if (DEBUG_MODE)
                        ex.printStackTrace();
                    break;
                }
                temp = this.lookup(temp, this.nodeID);
            }

            if (DEBUG_MODE) {
                System.out.println("DONE WITH BOOTSTRAPPING");
                routingTable.displayTable();
            }
        } catch (UnknownHostException | NullPointerException ex) {
            if (DEBUG_MODE)
                ex.printStackTrace();
            System.out.println("Something went wrong");
        }

        // Start showing options in the main thread
        new Node(id, 6).start();
    }

    // Listener threads
    Node(int id, int type) {
        nodeID = id;
        this.threadType = type;
    }

    // Other Ops threads
    Node(int id, int type, int ops_Type, NodeInfo from, int key) {
        this.nodeID = id;
        this.threadType = type;
        this.opsType = ops_Type;
        this.searchFrom = from;
        this.searchKey = key;
    }

    /**
     * Lookup Request Processing Thread
     *
     * @param id          ID of this node
     * @param type        Type of this thread
     * @param search_from Request originated from
     * @param search_key  Requesting key
     */
    Node(int id, int type, NodeInfo search_from, int search_key) {
        nodeID = id;
        this.threadType = type;
        this.searchFrom = search_from;
        this.searchKey = search_key;
        this.start();

        // Checking and sending back the file that was handled by "this" node in the requesting node's absence
        if (storedFiles.size() > 0) {
            List<Integer> delete_keys = new ArrayList<>();
            storedFiles.keySet().parallelStream().forEach(key -> {
                if ((key ^ this.nodeID) > (key ^ search_from.nodeId)) {
                    System.out.print("Trying to give back file with key: " + key + " to " + search_from.nodeId);
                    boolean success = false;
                    int retries = RETRY;
                    while (!success && retries > 0) {
                        success = this.store(searchFrom, storedFiles.get(key), key);
                        retries--;
                    }
                    if (success) {
                        // Deleting the file from our location
                        delete_keys.add(key);
                        System.out.println(" -- DONE ");
                    } else
                        System.err.println(" -- Something went wrong");
                }
            });
            delete_keys.forEach(storedFiles::remove);
        }
    }

    /**
     * Listener thread for the lookup
     */
    @SuppressWarnings("InfiniteLoopStatement")
    public void listenLookupRequest() {
        byte[] bytes = new byte[8];
        DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
        while (true) {
            try (DatagramSocket listen = new DatagramSocket(LOOKUP_REQUEST_PORT)) {
                listen.receive(packet);
                int from_id = bytes[3] << 24 | (bytes[2] & 0xFF) << 16 | (bytes[1] & 0xFF) << 8 | (bytes[0] & 0xFF);
                int search_key = bytes[7] << 24 | (bytes[6] & 0xFF) << 16 | (bytes[5] & 0xFF) << 8 | (bytes[4] & 0xFF);
                // starting request processing thread
                new Node(nodeID, 2, new NodeInfo(packet.getAddress(), LOOKUP_REQUEST_PORT, from_id), search_key);
            } catch (IOException | NumberFormatException ex) {
                if (DEBUG_MODE)
                    ex.printStackTrace();
                System.out.println("Something went wrong");
            }
        }
    }

    /**
     * Listener thread to listen for any file transfer
     */
    @SuppressWarnings("InfiniteLoopStatement")
    public void listenForFile() {
        try (ServerSocket listen = new ServerSocket(FILE_TRANSFER_PORT)) {
            ObjectInputStream in;
            while (true) {
                Socket skt = listen.accept();
                in = new ObjectInputStream(skt.getInputStream());
                int key = in.readInt();
                File file = (File) in.readObject();
                storedFiles.put(key, file);
                skt.close();
            }
        } catch (IOException | ClassNotFoundException ex) {
            if (DEBUG_MODE)
                ex.printStackTrace();
            System.out.println("Something went wrong");
        }
    }

    /**
     * Listener thread for any handling of the other ops based on ops_id
     */
    @SuppressWarnings("InfiniteLoopStatement")
    public void listenOtherOps() {
        byte[] bytes = new byte[12];
        DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
        while (true) {
            try (DatagramSocket in = new DatagramSocket(OTHER_OPS_REQUEST_PORT)) {
                in.receive(packet);
                int from_id = bytes[3] << 24 | (bytes[2] & 0xFF) << 16 | (bytes[1] & 0xFF) << 8 | (bytes[0] & 0xFF);
                int ops_id = bytes[7] << 24 | (bytes[6] & 0xFF) << 16 | (bytes[5] & 0xFF) << 8 | (bytes[4] & 0xFF);
                int key = bytes[11] << 24 | (bytes[10] & 0xFF) << 16 | (bytes[9] & 0xFF) << 8 | (bytes[8] & 0xFF);
                NodeInfo requestFrom = new NodeInfo(packet.getAddress(), OTHER_OPS_RESPONSE_PORT, from_id);
                new Node(this.nodeID, 5, ops_id, requestFrom, key).start();
            } catch (IOException ex) {
                if (DEBUG_MODE)
                    ex.printStackTrace();
                System.out.println("Something went wrong");
            }
        }
    }

    /**
     * Pinging known node in the routing table to check if the node is alive
     *
     * @param node_Id Pinging ID
     * @return true if node is alive else false
     */
    boolean ping(NodeInfo node_Id) {
        //NodeInfo node_Id = routingTable.getNodeInfo(node_Id);
        if (node_Id == null) {
            System.out.println("Unknown Node key");
            return false;
        }

        byte[] data = new byte[12];
        // Adding my Node ID information
        for (int index = 0; index < 4; index++)
            data[index] = (byte) (nodeID >> (8 * index));

        // Adding ops_id
        data[4] = 1;

        try (DatagramSocket socket = new DatagramSocket()) {
            DatagramPacket packet = new DatagramPacket(data, data.length, node_Id.ipAddress, OTHER_OPS_REQUEST_PORT);
            boolean success = false;
            int retries = RETRY;

            while (!success && retries > 0) {
                socket.send(packet);
                // Receive response
                try (DatagramSocket rcv_socket = new DatagramSocket(OTHER_OPS_RESPONSE_PORT)) {
                    rcv_socket.setSoTimeout(TIMEOUT);
                    rcv_socket.receive(packet);
                    success = true;
                } catch (IOException ex) {
                    ex.printStackTrace();
                } finally {
                    retries--;
                }
            }
            return success;
        } catch (IOException ex) {
            if (DEBUG_MODE)
                System.out.println("TIMEOUT AT PING");
            //ex.printStackTrace();
            System.out.println("Something went wrong");
        }
        return false;
    }

    /**
     * Processing other requests
     */
    public void processOtherOps() {
        byte[] data;
        DatagramPacket packet;
        switch (this.opsType) {
            case 1:
                // Refreshing the table from the ping request
                routingTable.refreshTable(searchFrom);
                data = new byte[4];
                // Adding my Node ID information
                for (int index = 0; index < 4; index++)
                    data[index] = (byte) (nodeID >> (8 * index));
                packet = new DatagramPacket(data, data.length, searchFrom.ipAddress, searchFrom.udpPort);
                try (DatagramSocket socket = new DatagramSocket()) {
                    socket.send(packet);
                    System.out.println("Sent Ping Response");
                } catch (IOException ex) {
                    if (DEBUG_MODE)
                        ex.printStackTrace();
                }
                break;
            case 2:
                boolean status = false;
                System.out.println("Searching for key:" + searchKey);
                System.out.println(storedFiles);

                if (storedFiles.containsKey(searchKey)) {
                    int retry = RETRY;
                    while (!status && retry > 0) {
                        status = this.store(searchFrom, storedFiles.get(searchKey), searchKey);
                        retry--;
                    }
                }

                // Sending status to the requester
                data = new byte[1];
                data[0] = (byte) (status ? 1 : 0);
                packet = new DatagramPacket(data, data.length, searchFrom.ipAddress,
                        OTHER_OPS_RESPONSE_PORT);
                try (DatagramSocket socket = new DatagramSocket()) {
                    socket.send(packet);
                } catch (IOException ex) {
                    if (DEBUG_MODE)
                        ex.printStackTrace();
                }
                break;
            case 3:
                /* Node leaving the network operation */
                try {
                    System.out.println("Exiting");
                    routingTable.remove(searchFrom);
                    sleep(TIMEOUT);
                    this.findNode(searchFrom.nodeId);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            default:
                // Handling unknown ops ID
                System.err.println("UNKNOWN OPS TYPE");
        }
    }

    /**
     * looking up a node in the network
     *
     * @param sendingNode Node to which the request is being sent
     * @param key Looking key
     * @return Nearest node to the key
     */
    NodeInfo lookup(NodeInfo sendingNode, int key) {
        byte[] packet = new byte[8];
        byte[] rcvd_data = new byte[4];
        NodeInfo returnObj = null;
        int retry = RETRY;

        // Adding my Node ID information
        for (int index = 0; index < 4; index++)
            packet[index] = (byte) (nodeID >> (8 * index));

        // Adding search key information to the packet
        for (int index = 0; index < 4; index++)
            packet[index + 4] = (byte) (key >> (8 * index));

        boolean success = false;
        while (!success && retry > 0) {
            try (DatagramSocket send_socket = new DatagramSocket();
                 DatagramSocket receive_socket = new DatagramSocket(LOOKUP_RESPONSE_PORT)) {
                receive_socket.setSoTimeout(TIMEOUT);

                // Creating packets to send and receive back
                DatagramPacket send_packet = new DatagramPacket(packet, packet.length,
                        sendingNode.ipAddress, LOOKUP_REQUEST_PORT);
                DatagramPacket rcvd_packet = new DatagramPacket(rcvd_data, rcvd_data.length);

                // Requesting for lookup
                send_socket.send(send_packet);
                // Listening for response size
                receive_socket.receive(rcvd_packet);

                int len = 0;
                for (int i = 0; i < 4; ++i)
                    len |= (rcvd_data[3 - i] & 0xff) << (i << 3);

                // Listening for response
                rcvd_data = new byte[len];
                rcvd_packet = new DatagramPacket(rcvd_data, rcvd_data.length);
                receive_socket.receive(rcvd_packet);

                ObjectInputStream oos = new ObjectInputStream(new ByteArrayInputStream(rcvd_data));

                // Casting received object to the required object type
                returnObj = (NodeInfo) oos.readObject();

                // Marking the process as success
                success = true;
            } catch (IOException | ClassNotFoundException ex) {
                if (DEBUG_MODE)
                    ex.printStackTrace();
                System.out.println("Something went wrong");
            } finally {
                retry--;
            }
        }
        if (DEBUG_MODE)
            if (returnObj != null && returnObj.nodeId != key)
                System.out.println("Search node doesn't exist in network. Nearest node found is " + returnObj);

        return returnObj;
    }

    /**
     * Finds the node that matches the key
     *
     * @param key search key
     * @return Nearest node to the key
     */
    NodeInfo findNode(int key) {
        NodeInfo lookup_node = routingTable.getNodeInfo(key);

        if (lookup_node == null)
            lookup_node = this.lookup(anchornode1, key);
        if (lookup_node == null)
            lookup_node = this.lookup(anchornode2, key);

        if (lookup_node == null) {
            if (DEBUG_MODE)
                System.out.println("KEY UNKNOWN TO ANCHOR NODES TOO!!!");
            return RoutingTable.myNode;
        } else
            routingTable.refreshTable(lookup_node);

        NodeInfo prev_node = RoutingTable.myNode;
        while (lookup_node.nodeId != this.nodeID) {
            if (!this.ping(lookup_node)) {
                routingTable.remove(lookup_node);
                lookup_node = prev_node;
                break;
            }
            prev_node = lookup_node;
            lookup_node = this.lookup(lookup_node, key);
            if (prev_node.nodeId == lookup_node.nodeId)
                break;
        }
        if (DEBUG_MODE)
            System.out.println("FOUND " + lookup_node);
        return lookup_node;
    }

    /**
     * Finding the value in the key
     *
     * @param key value's key
     * @return Value for the key
     */
    File findValue(int key) {
        NodeInfo searchNode = findNode(key % TOTAL_NODES);

        // No communication required if the file has to be fetched from myself
        if (searchNode.nodeId == this.nodeID)
            return storedFiles.getOrDefault(key, null);

        // Create packet to request file with given key
        byte[] data = new byte[12];
        // Adding my Node ID information
        for (int index = 0; index < 4; index++)
            data[index] = (byte) (nodeID >> (8 * index));

        // Adding ops_id
        for (int index = 0; index < 4; index++)
            data[index + 4] = (byte) (2 >> (8 * index));

        // Adding search key
        for (int index = 0; index < 4; index++)
            data[index + 8] = (byte) (key >> (8 * index));

        DatagramPacket sendPacket = new DatagramPacket(data, data.length, searchNode.ipAddress, OTHER_OPS_REQUEST_PORT);
        boolean success = false;
        int retries = RETRY;
        boolean status = false;
        while (!success && retries > 0) {
            try (DatagramSocket send_socket = new DatagramSocket();
                 DatagramSocket rcv_socket = new DatagramSocket(OTHER_OPS_RESPONSE_PORT)) {
                rcv_socket.setSoTimeout(TIMEOUT);
                send_socket.setSoTimeout(TIMEOUT);

                // Sending the file
                send_socket.send(sendPacket);
                success = true;

                // Waiting for the file
                System.out.println("Waiting to receive information");
                data = new byte[1];
                DatagramPacket packet = new DatagramPacket(data, data.length);
                rcv_socket.receive(packet);
                status = data[0] != 0;
            } catch (IOException e) {
                if (DEBUG_MODE)
                    e.printStackTrace();
                System.out.println("Something went wrong while receiving response");
            } finally {
                retries--;
            }
        }

        if (status)
            System.out.println("File found and received");
        else
            System.out.println("File not present in the network");

        return storedFiles.getOrDefault(key, null);
    }

    /**
     * Node leaving the network
     */
    void exit() {
        if (storedFiles.size() == 0)
            System.out.println(" *** Leaving Network *** ");
        else {
            System.out.print("Handing over my files");
            this.republish();
            System.out.println(" -- DONE");
        }

        byte[] data = new byte[12];
        // Adding my Node ID information
        for (int index = 0; index < 4; index++)
            data[index] = (byte) (nodeID >> (8 * index));

        // Adding ops_id
        for (int index = 0; index < 4; index++)
            data[4 + index] = (byte) (3 >> (8 * index));

        // Delete key
        for (int index = 0; index < 4; index++)
            data[8 + index] = (byte) (this.nodeID >> (8 * index));

        try (DatagramSocket socket = new DatagramSocket()) {
            DatagramPacket exit_packet = new DatagramPacket(data, data.length,
                    anchornode1.ipAddress, OTHER_OPS_REQUEST_PORT);
            socket.send(exit_packet);

            exit_packet = new DatagramPacket(data, data.length,
                    anchornode2.ipAddress, OTHER_OPS_REQUEST_PORT);
            socket.send(exit_packet);
        } catch (IOException ignored) {
        }

        System.out.println("Bye");
    }

    /**
     * Handing over all the documents when leaving the network
     */
    void republish() {
        routingTable.getAllNodes().forEach(nodeInfo -> storedFiles.keySet().forEach(key -> {
            if (key != this.nodeID)
                this.store(nodeInfo, storedFiles.get(key), key);
            if (DEBUG_MODE)
                System.out.println("Sent to " + nodeInfo);
        }));
    }

    /**
     * Store a file in given node with the given key
     *
     * @param storeIn Node in which the file has to be stored
     * @param file file to store
     * @param key key with which it has to be stored
     * @return true if success
     */
    public boolean store(NodeInfo storeIn, File file, int key) {
        try (Socket output = new Socket(storeIn.ipAddress, FILE_TRANSFER_PORT);
             ObjectOutputStream out = new ObjectOutputStream(output.getOutputStream())) {
            // Sending the key
            out.writeInt(key);
            out.flush();

            // Sending the file
            out.writeObject(file);
            out.flush();
        } catch (IOException ex) {
            if (DEBUG_MODE)
                ex.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void run() {
        switch (this.threadType) {
            case 1:
                // Starting the listening thread
                this.listenLookupRequest();
                break;
            case 2:
                // Searching the key and returning the nearest NodeInfo which might have the key
                NodeInfo node = routingTable.findKey(searchFrom, searchKey);

                if (node == null)
                    node = this.lookup(anchornode1, searchKey);
                if (node == null)
                    node = this.lookup(anchornode2, searchKey);
                if (node == null)
                    node = RoutingTable.myNode;

                try (DatagramSocket socket = new DatagramSocket();
                     ByteArrayOutputStream baos = new ByteArrayOutputStream();
                     ObjectOutputStream oos = new ObjectOutputStream(baos)) {

                    oos.writeObject(node);
                    oos.flush();

                    // Object's byte array
                    byte[] bytes = baos.toByteArray();

                    int number = bytes.length;
                    byte[] objLen = new byte[4];

                    // int -> byte[]
                    for (int i = 0; i < 4; ++i) {
                        int shift = i << 3;
                        objLen[3 - i] = (byte) ((number & (0xff << shift)) >>> shift);
                    }

                    DatagramPacket packet = new DatagramPacket(objLen, 4, searchFrom.ipAddress, LOOKUP_RESPONSE_PORT);
                    socket.send(packet);

                    // Sending the object
                    packet = new DatagramPacket(bytes, bytes.length, searchFrom.ipAddress, LOOKUP_RESPONSE_PORT);
                    socket.send(packet);
                } catch (IOException ex) {
                    if (DEBUG_MODE)
                        ex.printStackTrace();
                }
                break;
            case 3:
                this.listenForFile();
                break;
            case 4:
                this.listenOtherOps();
                break;
            case 5:
                this.processOtherOps();
                break;
            case 6:
                this.showOptions();
                break;
        }
    }

    /**
     * Giving options to the users
     */
    public void showOptions() {
        int option;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("**** PLEASE ENTER THE INDEX OF THE OPTION **** \n" +
                    "1. PING \n" +
                    "2. Show list of managed keys \n" +
                    "3. LookupNode \n" +
                    "4. FindValue \n" +
                    "5. Show my routing table \n" +
                    "6. Exit");
            try {
                option = Integer.parseInt(in.readLine());
                switch (option) {
                    case 1:
                        // Handle Pinging case
                        System.out.println("Please enter the node id to ping");
                        option = Integer.parseInt(in.readLine());
                        NodeInfo s_node = routingTable.getNodeInfo(option);
                        if (s_node != null && this.ping(s_node))
                            System.out.println("Node exists in my routing table");
                        else
                            System.out.println("Node is unknown or offline");
                        break;
                    case 2:
                        // Show list of keys being managed
                        System.out.println("I have following keys:");
                        for (int key : storedFiles.keySet())
                            System.out.println(key);
                        break;
                    case 3:
                        System.out.println("Please enter the node id to lookup");
                        option = Integer.parseInt(in.readLine());
                        System.out.println(this.findNode(option));
                        break;
                    case 4:
                        System.out.println("Please enter the key to find value");
                        option = Integer.parseInt(in.readLine());
                        this.findValue(option);
                        break;
                    case 5:
                        routingTable.displayTable();
                        break;
                    case 6:
                        System.out.println("Exiting... Bye");
                        this.exit();
                        System.exit(1);
                        break;
                    default:
                        System.out.println("Wrong input. Please check the options again");
                }
            } catch (IOException | NumberFormatException ex) {
                if (DEBUG_MODE)
                    ex.printStackTrace();
                System.out.println("Please check the input provided");
            }
        }
    }

    /**
     * @param file File to place inside the network
     * @return Hashcode of the file
     */
    public int putFile(File file) {
        int hashCode = Math.abs(file.hashCode());
        int file_key = Math.abs(file.hashCode()) % TOTAL_NODES;
        NodeInfo put_at = this.findNode(file_key);
        System.out.println("STORING " + hashCode + " IN: " + put_at.nodeId + " with FILE_KEY:" + file_key);
        this.store(put_at, file, hashCode);
        return hashCode;
    }

    /**
     * @param hashCode Hashcode of the file to lookup in the network
     * @return File
     */
    public File getFile(int hashCode) {
        boolean fileFromMyNode = storedFiles.containsKey(hashCode);
        File file = this.findValue(hashCode);
        if (!fileFromMyNode)
            storedFiles.remove(hashCode);
        return file;
    }
}
