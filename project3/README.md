## Instructions to run PubSub system

Move to the directory of the project using the change directory command 
```shell script
cd <project directory>
```

Build the image and start the anchor node containers, Node2 and Node4 using the below command.
This creates csci652 image and starts containers for Node2 and Node4. These containers are used 
by the other nodes to join the network  
```shell script
docker-compose -f docker-compose-project3.yml -p ds up --build -d
```

Open a new terminal window. Connect to Node2 and start the node using the below commands
```shell script
docker exec -it Node2 /bin/bash
java -cp project3/target/project3-1.0-SNAPSHOT.jar edu.rit.cs.KademliaNode 2
```

Open another new terminal window. Connect to Node4 and start the node using the below commands
```shell script
docker exec -it Node4 /bin/bash
java -cp project3/target/project3-1.0-SNAPSHOT.jar edu.rit.cs.KademliaNode 4
```


Above commands setup the Kademlia network with two anchor nodes. To start other nodes
use the below commands

```shell script
docker run -it --network=ds_csci652network csci652:latest bash
java -cp project3/target/project3-1.0-SNAPSHOT.jar edu.rit.cs.KademliaNode <NodeID>
```
 
All the above Node containers provide access to a few features like node leaving the network to show 
the routing table or to ping some other node. These onscreen options can be used to interact with 
Kademlia node

Above commands setup the Kademlia network with two anchor nodes. To start other nodes
use the below commands

To start a client that interacts with the entire network, below commands are used
```shell script
docker run -it --network=ds_csci652network csci652:latest bash
java -cp project3/target/project3-1.0-SNAPSHOT.jar edu.rit.cs.Client
```
Above commands creates a container and starts a client. This client's IP address is hashed and 
connected to Node2 or Node4 whichever is the nearest. It shows the three options to insert the document 
into the network or to retrieve the document from the network

