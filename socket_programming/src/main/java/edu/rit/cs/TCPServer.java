package edu.rit.cs;

import edu.rit.cs.basic_word_count.WordCount_Seq_Improved;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;


public class TCPServer {

    /**
     * Startign the server
     *
     * @param serverPort port of the server
     */
    public static void beginServer(int serverPort) {
        try {
            //int serverPort = 7896;
            // Starting the server
            ServerSocket listenSocket = new ServerSocket(serverPort);
            System.out.println("TCP Server is running and accepting client connections...");

            while (true) {
                // Creating thread worker for each request
                Socket s = listenSocket.accept();
                new Connection(s);
            }
        } catch (IOException e) {
            System.out.println("Listen :" + e.getMessage());
        }
    }
}

class Connection extends Thread {

    Socket clientSocket;
    TransferFile file;
    static final String file_dir = "socket_programming/files/";

    /**
     * Takes new request from the client and establishes input and output streams
     *
     * @param aClientSocket client communication socket
     */
    public Connection(Socket aClientSocket) {
        System.out.println("Receiving packets...");
        try {
            clientSocket = aClientSocket;
            this.start();
        } catch (Exception e) {
            System.out.println("Connection:" + e.getMessage());
        }
    }

    /**
     * Cleaning up files that are collected
     */
    public void cleanup() {
        try {
            new File(file_dir + "count.txt").deleteOnExit();
            new File(file_dir + "count.txt").deleteOnExit();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Thread that recieves, processes and returns the files
     */
    public void run() {
        try {
            file = new TransferFile(clientSocket);
            // Receiving the file from client socket
            file.receiveFile(file_dir + "Reviews.csv");
            System.out.println("Counting the Words");
            // Counting the words
            Map<String, Integer> wordCount =
                    WordCount_Seq_Improved.count_words(file_dir + "Reviews.csv");

            System.out.println("Creating the result file");
            // Writing the result to a file
            WordCount_Seq_Improved.create_result_file(wordCount, file_dir + "count.txt");

            System.out.println("Sending the result file to client");
            // Sending the response to client
            file.sendFile(new File(file_dir + "count.txt"));
            System.out.println("File sent to client successfully");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            System.out.println("Cleaning up");
            this.cleanup();
            try {
                // Closing the connections
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
