package edu.rit.cs;

import java.io.*;
import java.net.Socket;

public class TransferFile {
    File file;
    BufferedOutputStream bos;
    BufferedInputStream bis;
    Socket sock;

    /**
     * @param socket
     */
    TransferFile(Socket socket) {
        this.sock = socket;
    }

    /**
     * Send the file
     *
     * @param transferFile
     * @return
     */
    public boolean sendFile(File transferFile) {
        int n = 1024;
        this.file = transferFile;
        byte[] buf = new byte[n];
        int packet = 0;

        try (BufferedInputStream fis = new BufferedInputStream(new FileInputStream(file))) {
            // Sending the FileSize
            new DataOutputStream(sock.getOutputStream()).writeLong(transferFile.length());

            // Output Stream for the transfer
            this.bos = new BufferedOutputStream(sock.getOutputStream());
            while ((n = fis.read(buf, 0, buf.length)) != -1) {
                packet++;
                bos.write(buf, 0, n);
                bos.flush();
            }
            //bos.close();
            System.out.println("Sent packets:" + packet);
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     *  Receive the file
     * @param file_name
     * @return
     */
    public boolean receiveFile(String file_name) {
        int n = 1024;
        byte[] buf = new byte[n];
        FileOutputStream fos;
        long bytes_received = 0;
        try {
            // Read the input file size
            long file_size = new DataInputStream(sock.getInputStream()).readLong();
            System.out.println("File size to receive :" + file_size);

            // Opening the stream to write the contents to a file
            fos = new FileOutputStream(file_name);
            this.bis = new BufferedInputStream(sock.getInputStream());
            BufferedOutputStream b = new BufferedOutputStream(fos);
            int packets = 0;

            // Receiving the packets
            while ((n = bis.read(buf)) != -1) {
                bytes_received += n;
                b.write(buf, 0, n);
                packets++;
                b.flush();
                // Received entire file
                if (bytes_received >= file_size)
                    break;
            }
            b.close();
            System.out.println("File Received successfully. Bytes Received:" + bytes_received);
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

}