package edu.rit.cs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;

public class TCPClient {

    static String file_dir;
    static Socket socket;

    /**
     * Client object with the location of the file
     *
     * @param dir
     */
    TCPClient(String dir) {
        this.file_dir = dir;
    }

    /**
     * Checks if server is online and forms a socket connection
     *
     * @param SERVER_ADDRESS  Host address
     * @param TCP_SERVER_PORT port number
     * @return True if server is online
     */
    public static boolean isServerOnline(String SERVER_ADDRESS, int TCP_SERVER_PORT) {
        try {
            socket = new Socket(SERVER_ADDRESS, TCP_SERVER_PORT);
            return true;
        } catch (IOException ex) {
            // Server is not up yet
            return false;
        }
    }

    /**
     * Prints the counts received from the server
     */
    public static void show_counts() {
        String currLine;
        // Read each line from the file and print
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file_dir + "count.txt"))) {
            while ((currLine = bufferedReader.readLine()) != null)
                System.out.println(currLine);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Starting the client
     *
     * @param server_address
     * @param serverPort
     * @throws IOException
     */
    public void startClient(String server_address, int serverPort) throws IOException {
        // arguments supply message and hostname of destination
        TransferFile transfer;
        File f;

        try {
            while (!isServerOnline(server_address, serverPort)) {
                System.out.println("Waiting for server to come online");
                // Waiting
                try {
                    // Waiting for a second
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    continue;
                }
            }

            f = new File(file_dir + "affr.csv");

            transfer = new TransferFile(socket);
            if (transfer.sendFile(f)) {
                System.out.println("Fine Sent Successfully");
                if (transfer.receiveFile(file_dir + "count.txt")) {
                    System.out.println("Received file with counts ");
                    show_counts();
                } else {
                    System.out.println("Error while receiving the file");
                }
            } else {
                System.out.println("Communication with Server failed");
            }
        } finally {
            if (socket != null)
                socket.close();
        }
    }
}