package edu.rit.cs;

public class Server {
    /**
     * Main server program
     *
     * @param args Commandline arguments
     */
    public static void main(String[] args) {
        try {
            // Starting the server
            TCPServer.beginServer(Integer.parseInt(args[0]));
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
    }
}
