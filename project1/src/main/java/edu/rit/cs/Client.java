package edu.rit.cs;

import java.io.IOException;

public class Client {
    /**
     * Main client program
     *
     * @param args Commandline Arguments
     */
    public static void main(String[] args) {
        // Reading the host address from the command lines
        String host = args[0];
        TCPClient client = new TCPClient("project1/files/");
        try {
            int port = 7896;
            // Starting the client
            client.startClient(host, port);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
