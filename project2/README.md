## Instructions to run PubSub system

Move to the directory of the project using the change directory command 
```
cd <project directory>
```

Build the image and start the Event Manager container using the below command.
This creates csci652 image and starts event manager  
```
docker-compose -f docker-compose-project2.yml -p ds up --build -d
```

Open a new terminal window. Other client containers which can act either as a subscriber or publisher can be started using the below command.
This starts a container and connects to the same network that is created by the dockerfile. Hence the communication
is established between the clients and Event Manager. This step is repeated as many times as the number of publishers and
subscribers are required
  
```
docker run -it --network=ds_csci652network csci652:latest bash
```

Above command creates a container and opens a terminal. The below command starts the PubSubMain class which
takes eventManager hostname and ID of the subscriber. If starting for the first time, ID is gives as a negative number.
```
java -cp project2/target/project2-1.0-SNAPSHOT.jar edu.rit.cs.PubSubMain eventManager [<Pub/Sub ID>]
```