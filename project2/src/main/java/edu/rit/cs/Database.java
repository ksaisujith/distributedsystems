package edu.rit.cs;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Database class to store all the required information
 */
public class Database {

    // Keeps track of active clients
    public static PriorityBlockingQueue<Client> alive_clients = new PriorityBlockingQueue<>(10, Comparator.comparingLong(o -> o.heartbeat_time));
    private static int current_topic_id = 0;
    // Generates the IDs for topic and client
    private static int current_client_id = 0;
    // Storage of List of all topics
    private static List<Topic> topicList = new CopyOnWriteArrayList<>();

    // Storage of subscribers who subscribed to a topic (topicID, Subscriber Id)
    private static ConcurrentHashMap<Integer, List<Integer>> topic_sub_map = new ConcurrentHashMap<>();

    // Storage of events that are pending events to deliver for a subscriber (subscriber_id , events_to_deliver)
    private static ConcurrentHashMap<Integer, List<Event>> pending_event_delivery = new ConcurrentHashMap<>();

    // Storage of events that are pending topics to deliver for a subscriber (subscriber_id , topics_to_deliver)
    private static ConcurrentHashMap<Integer, List<Topic>> pending_topic_delivery = new ConcurrentHashMap<>();

    // Storage of clients and their ID mapped (clientID, Clientobject)
    private static ConcurrentHashMap<Integer, Client> clients = new ConcurrentHashMap<>();

    //****** Getter Functions
    // returns the subscribers for the topics
    public static ConcurrentHashMap<Integer, List<Integer>> getTopic_sub_map() {
        return topic_sub_map;
    }

    // Returns new topic ID
    public synchronized int getNewTopicID() {
        return ++current_topic_id;
    }

    // Return new client ID
    public synchronized int getNewClientID() {
        return ++current_client_id;
    }

    /**
     * Check if Topic exists in Event Manager
     *
     * @param topicID ID of the topic to check
     * @return True if topicID exists at EventManager
     */
    public boolean topicExists(int topicID) {
        return topic_sub_map.containsKey(topicID);
    }

    /**
     * Client exists in EventManager
     *
     * @param clientID ID of the client
     * @return True if ID exists
     */
    public boolean clientExists(int clientID) {
        return clients.containsKey(clientID);
    }

    /**
     * Add clients to the event manager and update IP address
     *
     * @param id ID of the clinet
     * @param IP IP address of the client
     */
    public void addClient(int id, String IP) {
        Client client;

        if (clients.containsKey(id)) {
            // Updating the heartbeat time and IP address
            client = clients.get(id);
            // Updating time and IP address
            client.setTime(System.currentTimeMillis());
            client.setIP_address(IP);

            // Removing client from the alive clients
            alive_clients.remove(client);
        } else {
            // Creating the client ID
            client = new Client(id, IP, System.currentTimeMillis());
            clients.put(id, client);
        }

        // Adding the client
        alive_clients.add(client);
    }

    /**
     * Get all the list of topics in event manager
     *
     * @return List of topics
     */
    public List<Topic> getTopics() {
        return topicList;
    }

    /**
     * Adds new topic to the event manager
     *
     * @param topic New topic to add
     */
    public void addNewTopic(Topic topic) {
        topicList.add(topic);
        topic_sub_map.put(topic.getId(), new ArrayList<>());
    }

    /**
     * Add a subscriber to a topic
     *
     * @param subID   Subscriber
     * @param topicID Topic to be added
     * @return True if success
     */
    public boolean addSubToTopic(int subID, int topicID) {
        if (topic_sub_map.containsKey(topicID)) {
            topic_sub_map.get(topicID).add(subID);
            return true;
        } else
            return false;
    }

    /**
     * Remove a subscriber from a topic
     *
     * @param subID   subscriber id
     * @param topicID Topic ID to be removed from
     * @return True if success
     */
    public boolean removeSubFromTopic(int subID, int topicID) {
        if (topic_sub_map.containsKey(topicID)
                && topic_sub_map.get(topicID).contains(subID)) {
            topic_sub_map.get(topicID).remove(new Integer(subID));
            return true;
        }
        return false;
    }

    /**
     * Returns a subscriber for the topic
     *
     * @param topicId Topic to be searched
     * @return list of subscriber IDs
     */
    public List<Integer> getSubscribersForTopic(int topicId) {
        return topic_sub_map.get(topicId);
    }

    /**
     * Add pending event delivery to the queue
     *
     * @param subscriberID ID of client to be sent to
     * @param newEvent     Event to be sent
     */
    public void addPendingDelivery(int subscriberID, Event newEvent) {
        if (!pending_event_delivery.containsKey(subscriberID))
            pending_event_delivery.put(subscriberID, new CopyOnWriteArrayList<>());
        pending_event_delivery.get(subscriberID).add(newEvent);
    }

    /**
     * Add pending event delivery to the queue
     *
     * @param newTopic Topic to be sent
     */
    public void addPendingDelivery(Topic newTopic) {
        for (int clientID : clients.keySet()) {
            if (!pending_topic_delivery.containsKey(clientID)) {
                pending_topic_delivery.put(clientID, new CopyOnWriteArrayList<>());
            }
            pending_topic_delivery.get(clientID).add(newTopic);
        }
    }

    /**
     * Delete the client who is inactive for a threshold time
     *
     * @param inactivity Threshold time
     */
    public void deleteInactiveClients(long inactivity) {
        if (alive_clients != null) {
            while (System.currentTimeMillis() -
                    (alive_clients.peek() != null ? alive_clients.peek().heartbeat_time : System.currentTimeMillis())
                    > inactivity) {
                Client c = alive_clients.poll();
                if (c != null) {
                    System.out.println("Deleted client with ID: " + c.id + " due to inactivity");
                }
            }
        }
    }

    /**
     * Mark a client as inactive
     *
     * @param subId ID of the client
     * @return True if successfully removed
     */
    public boolean markInactive(int subId) {
        if (alive_clients.contains(clients.get(subId))) {
            alive_clients.remove(clients.get(subId));
            return true;
        }
        return false;
    }

    /**
     * Get the iterator of the active client
     *
     * @return Iterator
     */
    public Iterator<Client> get_ActiveClients() {
        return alive_clients.iterator();
    }

    /**
     * Pending events list
     *
     * @return list of pending events
     */
    public ConcurrentHashMap<Integer, List<Event>> getPending_event_delivery() {
        return pending_event_delivery;
    }

    /**
     * Pending topics list
     *
     * @return list of pending topics
     */
    public ConcurrentHashMap<Integer, List<Topic>> getPending_topic_delivery() {
        return pending_topic_delivery;
    }
}
