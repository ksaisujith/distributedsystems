package edu.rit.cs;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.List;

public interface Publisher extends Remote {
    /*
     * publish an event of a specific topic with title and content
     */
    public boolean publish(Event event) throws RemoteException;

    /*
     * advertise new topic
     */
    public boolean advertise(Topic newTopic) throws RemoteException;

    /*
     * List of all topics along with their IDs
     */
    public List<Topic> listTopics() throws RemoteException;
}
