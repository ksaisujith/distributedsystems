package edu.rit.cs;

import java.io.Serializable;

/**
 * Event class for the
 */
public class Event implements Serializable {
    // Class variables
    private int topic_id;
    private String event_title;
    private String event_content;

    /**
     * Prints the object in this format
     *
     * @return String format
     */
    @Override
    public String toString() {
        return "Event{" +
                "Title:'" + event_title + '\'' +
                ", Content:'" + event_content + '\'' +
                '}';
    }

    /**
     * Set the event title
     *
     * @param title title to set the object
     */
    public void setEvent_title(String title) {
        this.event_title = title;
    }

    /**
     * Set the content title
     *
     * @param content Content of the event
     */
    public void setEvent_content(String content) {
        this.event_content = content;
    }

    /**
     * Get the topic ID
     *
     * @return Topic ID of the object
     */
    public int getTopic_id() {
        return topic_id;
    }

    /**
     * Sets the Id of the topic
     *
     * @param ID ID of the topic to set
     */
    public void setTopic_id(int ID) {
        this.topic_id = ID;
    }
}
