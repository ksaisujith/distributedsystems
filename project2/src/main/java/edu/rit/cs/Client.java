package edu.rit.cs;

/**
 * Client class to hold the details of Pub or Sub details
 */
public class Client {
    // Class variables to store client details
    final int id;
    String IP_address;
    long heartbeat_time;

    /**
     * Client constructor
     *
     * @param i    ID of the object
     * @param ip   IP address
     * @param time Time heartbeat received from client
     */
    public Client(int i, String ip, long time) {
        this.id = i;
        this.IP_address = ip;
        this.heartbeat_time = time;
    }

    /**
     * Format to print the client object
     *
     * @return
     */
    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", IP_address='" + IP_address + '\'' +
                ", heartbeat_time=" + heartbeat_time +
                '}';
    }

    /**
     * to set the time of the client object
     *
     * @param time Time
     */
    public void setTime(long time) {
        this.heartbeat_time = time;
    }

    /**
     * Updates the IP address
     *
     * @param ip_address IP address
     */
    public void setIP_address(String ip_address) {
        this.IP_address = ip_address;
    }
}