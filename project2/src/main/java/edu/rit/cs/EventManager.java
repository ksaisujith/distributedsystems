package edu.rit.cs;

import java.io.*;
import java.net.*;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Iterator;

/**
 * Class of event manager
 */
public class EventManager extends Thread {

	private static Database database = new Database();
	private static final int CLEANUP_TIME = 5000; // in milliseconds
	private static final long INACTIVITY_TIME = 1000; // in milliseconds
	private static final int RMI_PORT = 9999;
	private static final int HEARTBEAT_PORT = 11111;
	private static final int EVENT_NOTIFICATION_PORT = 9080;
	private static final int TOPIC_NOTIFICATION_PORT = 9081;
	private static final int Client_Connection_Port = 9090;
	private static final int QOS = 1; // 0 at most once; 1: gte 1; 2 : eq 1
	private int threadType;
	private Socket clientSocket;


	/**
	 * Event manager thread that handles different functions
	 *
	 * @param _threadType 1: TCP Connection Thread; 2: RMI Registry Thread 3:Event Managements 4: HeartBeat Tracker
	 *                    5: Client cleanup thread
	 */
	public EventManager(int _threadType) {
		this.threadType = _threadType;
	}

	/**
	 * Event manager object that listens from the socket
	 *
	 * @param socket New Socket to communicate with
	 */
	public EventManager(Socket socket) {
		clientSocket = socket;
		this.threadType = 3;
	}

	/**
	 * Main function
	 *
	 * @param args Commandline arguments
	 */
	public static void main(String[] args) {

		// Starting a thread to listen for new connections
		new EventManager(1).start();

		// Starting a thread to start RMI service
		new EventManager(2).start();

		// Starting a thread for listening heart beat
		new EventManager(4).start();

		// Starting a thread cleanup
		new EventManager(5).start();
	}

	/*
	 * Start the repo service
	 */
	private void startService() throws RemoteException {
		Registry registry;
		try {
			// Starting the RMI service
			registry = LocateRegistry.createRegistry(RMI_PORT);
			System.out.println("RMI registry created");
		} catch (RemoteException ex) {
			registry = LocateRegistry.getRegistry();
			System.out.println("RMI registry exists");
		}

		try {
			// Adding the objects to access
			Publisher pub = new PubSubAgent();
			Subscriber sub = new PubSubAgent();
			registry.rebind("Publisher", pub);
			registry.rebind("Subscriber", sub);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Run different threads
	 */
	public void run() {
		// Listening for connections
		if (threadType == 1)
			listen_for_connections();

		// Starting RMI service
		if (threadType == 2) {
			try {
				this.startService();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		if (threadType == 3)
			this.validateID();

		if (threadType == 4)
			this.record_heartBeat();

		if (threadType == 5)
			this.clientCleanup();

		if (threadType == 6)
			this.notifyEvents();

		if (threadType == 7)
			this.notifyTopics();
	}

	/**
	 *
	 */
	@SuppressWarnings("InfiniteLoopStatement")
	public void listen_for_connections() {
		System.out.println("Accepting new pub/sub connections on port:" + Client_Connection_Port);
		try (ServerSocket serverSocket = new ServerSocket(Client_Connection_Port)) {
			while (true) {
				Socket clientSocket = serverSocket.accept();
				new EventManager(clientSocket).start();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Validate the ID of the user
	 */
	public void validateID() {
		try {
			DataInputStream dis = new DataInputStream(this.clientSocket.getInputStream());
			DataOutputStream dos = new DataOutputStream(this.clientSocket.getOutputStream());

			int client_id = dis.readInt();
			System.out.println("Client ID validate" + client_id);
			client_id = database.clientExists(client_id) ? client_id : database.getNewClientID();
			dos.writeInt(client_id);

			database.addClient(client_id, clientSocket.getRemoteSocketAddress().toString().replace("/", "").split(":")[0]);
			notifyEvents();
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}

	/**
	 * Listen for heartbeats
	 */
	@SuppressWarnings("InfiniteLoopStatement")
	public void record_heartBeat() {
		try (DatagramSocket listen = new DatagramSocket(HEARTBEAT_PORT)) {
			byte[] bytes = new byte[4];
			DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
			while (true) {
				listen.receive(packet);
				String address = packet.getAddress().toString().replace("/", "");
				address = address.split(":")[0];
				int id = bytes[0] << 24 | (bytes[1] & 0xFF) << 16 | (bytes[2] & 0xFF) << 8 | (bytes[3] & 0xFF);
				id = database.clientExists(id) ? id : database.getNewClientID();
				database.addClient(id, address);
			}
		} catch (IOException | NumberFormatException ignored) { }
	}

	/**
	 * Notify the events
	 */
	public void notifyEvents() {
		// Check for active users and pending events for them
		Iterator<Client> activeClients = database.get_ActiveClients();
		while (activeClients.hasNext()) {
			Client client = activeClients.next();
			if (database.getPending_event_delivery().containsKey(client.id)) {
				try (Socket socket = new Socket(client.IP_address, EVENT_NOTIFICATION_PORT)) {
					ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
					out.writeInt(database.getPending_event_delivery().get(client.id).size());
					for (Event event : database.getPending_event_delivery().get(client.id)) {
						out.writeObject(event);
						out.flush();

						// Removing after sending the notification successfully
						database.getPending_event_delivery().get(client.id).remove(event);

						// Removing the ID for empty list
						if (database.getPending_event_delivery().get(client.id).size() == 0)
							database.getPending_event_delivery().remove(client.id);
					}
				} catch (IOException e) {
					if (QOS == 0) {
						for (Event event : database.getPending_event_delivery().get(client.id)) {

							// Removing notification
							database.getPending_event_delivery().get(client.id).remove(event);

							// Removing the ID for empty list
							if (database.getPending_event_delivery().get(client.id).size() == 0)
								database.getPending_event_delivery().remove(client.id);
						}
					}
				}
			}
		}
	}

	/**
	 * Notify the topics
	 */
	public void notifyTopics() {
		// Check for active users and pending Topics for them
		Iterator<Client> activeClients = database.get_ActiveClients();
		while (activeClients.hasNext()) {
			Client client = activeClients.next();
			if (database.getPending_topic_delivery().containsKey(client.id)) {
				try (Socket socket = new Socket(client.IP_address, TOPIC_NOTIFICATION_PORT)) {
					ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
					out.writeInt(database.getPending_topic_delivery().get(client.id).size());
					System.out.println("Items to send " + database.getPending_topic_delivery().get(client.id).size());
					for (Topic topic : database.getPending_topic_delivery().get(client.id)) {
						out.writeObject(topic);
						out.flush();

						// Removing after sending the notification successfully
						database.getPending_topic_delivery().get(client.id).remove(topic);

						// Removing the ID for empty list
						if (database.getPending_topic_delivery().get(client.id).size() == 0)
							database.getPending_topic_delivery().remove(client.id);
					}
				} catch (IOException e) {
					if (QOS == 0) {
						for (Topic topic : database.getPending_topic_delivery().get(client.id)) {
							// Removing notification
							database.getPending_topic_delivery().get(client.id).remove(topic);

							// Removing the ID for empty list
							if (database.getPending_topic_delivery().get(client.id).size() == 0)
								database.getPending_topic_delivery().remove(client.id);
						}
					}
				}
			}
		}
	}

	/**
	 * Removes non-connected users from the active users list
	 */
	@SuppressWarnings("InfiniteLoopStatement")
	public void clientCleanup() {
		while (true) {
			try {
				database.deleteInactiveClients(INACTIVITY_TIME);
				sleep(CLEANUP_TIME);
			} catch (InterruptedException ignore) {
			}
		}
	}
}
