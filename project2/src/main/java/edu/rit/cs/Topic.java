package edu.rit.cs;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

/**
 * Topic Object
 */
public class Topic implements Serializable {
    private int id;
    private List<String> keywords;
    private String name;

    public Topic(String _name) {
        this.name = _name;
        this.keywords = Arrays.asList(_name.split(" "));
    }

    public Topic(String _name, String[] keys) {
        this.name = _name;
        // Trimming the keywords
        for (int index = 0; index < keys.length; index++)
            keys[index] = keys[index].replace("[^A-Za-z0-9]", "");
        // Adding the keys
        this.keywords = Arrays.asList(keys);
    }

    @Override
    public String toString() {
        return "Topic{" +
                "id:" + id +
                ", keywords:" + keywords +
                ", name:'" + name + '\'' +
                '}';
    }

    public void setId(int ID) {
        this.id = ID;
    }

    public int getId() {
        return id;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public String getName() {
        return name;
    }
}
