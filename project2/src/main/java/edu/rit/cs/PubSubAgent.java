package edu.rit.cs;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class PubSubAgent extends UnicastRemoteObject implements Publisher, Subscriber {
    static Database database = new Database();

    /**
     * @throws RemoteException
     */
    protected PubSubAgent() throws RemoteException {
    }

    /**
     * @param subscriberId
     * @param TopicName
     * @return
     * @throws RemoteException
     */
    @Override
    public boolean subscribeByName(int subscriberId, String TopicName) throws RemoteException {
        if (database.clientExists(subscriberId)) {
            for (Topic topic : database.getTopics()) {
                if (topic.getName().equals(TopicName)) {
                    database.addSubToTopic(subscriberId, topic.getId());
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param subscriberId
     * @param topic_id
     * @return
     */
    @Override
    public boolean subscribe(int subscriberId, int topic_id) {
        if (database.clientExists(subscriberId)) {
            return database.addSubToTopic(subscriberId, topic_id);
        }
        return false;
    }

    /**
     * @param subscriberId
     * @param keyword
     * @return
     */
    @Override
    public boolean subscribe(int subscriberId, String keyword) {
        boolean added = false;
        if (database.clientExists(subscriberId)) {
            for (Topic topic : database.getTopics()) {
                if (topic.getKeywords().contains(keyword)) {
                    database.addSubToTopic(subscriberId, topic.getId());
                    added = true;
                }
            }
        }
        return added;
    }

    /**
     * @param subscriberId
     * @param topic_id
     * @return
     */
    @Override
    public boolean unsubscribe(int subscriberId, int topic_id) {
        if (database.clientExists(subscriberId)) {
            return database.removeSubFromTopic(subscriberId, topic_id);
        }
        return false;
    }

    /**
     * @param subscriberId
     * @return
     */
    @Override
    public boolean unsubscribe(int subscriberId) {
        ConcurrentHashMap<Integer, List<Integer>> topic_sub = Database.getTopic_sub_map();
        boolean success = false;
        for (int topic_id : topic_sub.keySet()) {
            if (topic_sub.get(topic_id).contains(subscriberId))
                success = success || database.removeSubFromTopic(subscriberId, topic_id);
        }
        return success;
    }

    /**
     * @param subscriberId
     * @return
     */
    @Override
    public List<Topic> listSubscribedTopics(int subscriberId) {
        List<Topic> result_list = new ArrayList<>();
        ConcurrentHashMap<Integer, List<Integer>> topic_sub = Database.getTopic_sub_map();
        for (int topic_id : topic_sub.keySet()) {
            if (topic_sub.get(topic_id).contains(subscriberId)) {
                for (Topic topic : database.getTopics()) {
                    if (topic.getId() == topic_id)
                        result_list.add(topic);
                }
            }
        }
        System.out.println("Subscribed TOpics");
        return result_list;
    }

    @Override
    public boolean offline(int subscriberId) throws RemoteException {
        return database.markInactive(subscriberId);
    }

    /**
     * @param event
     * @return
     */
    @Override
    public boolean publish(Event event) {
        // Checking if topic ID exists
        if (database.topicExists(event.getTopic_id())) {

            // Marking for delivery for each subscribed client
            for (int subID : database.getSubscribersForTopic(event.getTopic_id())) {
                database.addPendingDelivery(subID, event);
            }

            new EventManager(6).start();
            return true;
        }
        return false;
    }

    /**
     * @param newTopic
     * @return
     */
    @Override
    public boolean advertise(Topic newTopic) {
        // Checking if topic already exists
        for (Topic topic : database.getTopics()) {
            if (topic.getName().equals(newTopic.getName())) {
                // Topic exists
                return false;
            }
        }

        // Topic is new and assigning new ID to it
        newTopic.setId(database.getNewTopicID());
        database.addNewTopic(newTopic);
        database.addPendingDelivery(newTopic);

        new EventManager(7).start();
        return true;
    }

    /**
     * @return
     */
    @Override
    public List<Topic> listTopics() {
        System.out.println("Sending Topics list...");
        return database.getTopics();

    }

}
