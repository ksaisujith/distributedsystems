package edu.rit.cs;

import java.io.*;
import java.net.*;
import java.util.List;

public class PubSubMain extends Thread {
    static final int HEARTBEAT_RATE = 100;
    static final int EVENT_NOTIFICATION_PORT = 9080;
    static final int TOPIC_NOTIFICATION_PORT = 9081;
    static final int EVENTMANAGER_PORT = 9090;
    static final int HEARTBEAT_PORT = 11111;
    static final int RMI_PORT = 9999;
    static final int DELAY = 500;
    static Communicator communicator;
    int threadType;


    /**
     * @param id
     * @param hostname
     */
    public PubSubMain(int id, String hostname) {
        communicator = new Communicator(id, EVENTMANAGER_PORT, hostname, RMI_PORT, HEARTBEAT_PORT);
    }

    /**
     * Creating threads
     *
     * @param threadType 1:Event Listener thread, 2:heartbeat thread 3: Topic Listener thread
     */
    public PubSubMain(int threadType) {
        this.threadType = threadType;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        String host = null;
        int identifier = 0;

        // Parsing the arguments
        if (args.length == 2) {
            try {
                host = args[0];
                identifier = Integer.parseInt(args[1]);
            } catch (NumberFormatException ex) {
                show_error();
            }
        } else if (args.length == 1) {
            System.out.println("Please enter ID if known else type any negative value to generate");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            try {
                identifier = Integer.parseInt(in.readLine());
                host = args[0];
            } catch (NumberFormatException ex) {
                show_error();
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println(ex.getMessage());
            }
        } else {
            show_error();
        }
        // Starting Listener to receive Events
        try {
            System.out.print("Starting event listener thread");
            Thread eventsThread = new PubSubMain(1);
            eventsThread.start();
            eventsThread.join(DELAY);
            System.out.println(" -- DONE");

            // Starting Listener to receive Topics
            System.out.print("Starting topic listener thread");
            Thread topicsThread = new PubSubMain(3);
            topicsThread.start();
            topicsThread.join(DELAY);
            System.out.println(" -- DONE");
        } catch (InterruptedException ignored) {
        }

        new PubSubMain(identifier, host).start();
    }

    /**
     *
     */
    public static void show_error() {
        System.out.println("Wrong inputs provided \n" +
                "Usage: PubSubMain.java <hostname> <ID> \n" +
                "Type any negative number for ID if unknown\n");
        System.exit(1);
    }

    /**
     *
     */
    public void run() {

        // Starting to Listen for incoming UDP packets
        if (this.threadType == 1)
            this.listenForEvents();

        else if (this.threadType == 2)
            this.startHeartBeat();

        else if (this.threadType == 3)
            this.listenForTopics();

        // Request actions from the user
        startActions();
    }

    public void startActions() {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        int choice;
        while (true) {
            System.out.println("\n==============================================================\n" +
                    "Please enter the index number for the specific action\n" +
                    "==============================================================\n" +
                    "1. Show list all topics \n" +
                    "2. Subscribe Topic by Name \n" +
                    "3. Subscribe Topic by ID \n" +
                    "4. Subscribe Topic by keyword\n" +
                    "5. Unsubscribe by ID\n" +
                    "6. Unsubscribe from all the topics\n" +
                    "7. Show list of subscribed topics\n" +
                    "8. Publish an Event\n" +
                    "9. Advertise a Topic\n" +
                    "10. Exit");
            try {
                int topicID;
                String topicKey;
                choice = Integer.parseInt(input.readLine());

                switch (choice) {
                    case 1:
                        List<Topic> topics_list = communicator.getAllTopics();
                        System.out.println("Number of topics:" + topics_list.size());
                        for (Topic topic : topics_list)
                            System.out.println(topic);
                        break;
                    case 2:
                        System.out.println("Please enter Name of the topic that you want to subscribe");
                        topicKey = input.readLine();
                        communicator.subscribeByName(topicKey);
                        break;
                    case 3:
                        System.out.println("Please enter ID of the topic that you want to subscribe");
                        topicID = Integer.parseInt(input.readLine());
                        communicator.subscribeByID(topicID);
                        break;
                    case 4:
                        System.out.println("Please enter the key for the topic to subscribe");
                        topicKey = input.readLine();
                        communicator.subscribeByKey(topicKey);
                        break;
                    case 5:
                        System.out.println("Please enter ID of the topic that you want to unsubscribe");
                        topicID = Integer.parseInt(input.readLine());
                        communicator.unsubscribeByID(topicID);
                        break;
                    case 6:
                        communicator.unsubscribeAll();
                        break;
                    case 7:
                        List<Topic> subTopics = communicator.getSubscribedTopics();
                        System.out.println("Number of subscribed topics:" + subTopics.size());
                        for (Topic topic : subTopics)
                            System.out.println(topic);
                        break;
                    case 8:
                        Event newEvent = new Event();
                        System.out.println("Please enter the ID of the Topic for this event");
                        newEvent.setTopic_id(Integer.parseInt(input.readLine()));

                        System.out.println("Please enter the Title of the event");
                        newEvent.setEvent_title(input.readLine());

                        System.out.println("Please enter the content event");
                        newEvent.setEvent_content(input.readLine());
                        communicator.publish(newEvent);
                        break;
                    case 9:
                        System.out.println("Do you want to add keywords?(y/n)" +
                                "\nIf no, keywords are generated from the title by splitting");
                        String c = input.readLine();
                        System.out.println("Please enter Topic name");
                        String topic_name = input.readLine();
                        Topic newTopic;

                        if (c.toLowerCase().equals("y")) {
                            System.out.println("Please enter keywords seperated by coma(,)");
                            String[] keys = input.readLine().split(",");
                            for (int index = 0; index < keys.length; index++) {
                                keys[index] = keys[index].replace("[^A-Za-z0-9]", "");
                            }
                            newTopic = new Topic(topic_name, keys);
                        } else {
                            newTopic = new Topic(topic_name);
                        }
                        communicator.advertise(newTopic);
                        break;
                    case 10:
                        communicator.exit();
                        System.exit(0);
                    default:
                        System.out.println("Please enter a valid index from the list");
                }
            } catch (NumberFormatException ex) {
                System.out.println("Something went wrong!!! \n" +
                        "Please enter the integer index of the action from the list\n\n");
            } catch (NullPointerException | IOException ex) {
                System.out.println("Something went wrong!!! \n" +
                        "Please try again\n\n");
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    /**
     * Listener thread that receives the notifications
     */
    public void listenForEvents() {
        try (ServerSocket listen = new ServerSocket(EVENT_NOTIFICATION_PORT)) {
            while (true) {
                Socket s = listen.accept();
                new ProcessEvent(s).start();
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }


    /**
     * Listener thread that receives the notifications
     */
    public void listenForTopics() {
        try (ServerSocket listen = new ServerSocket(TOPIC_NOTIFICATION_PORT)) {
            while (true) {
                Socket s = listen.accept();
                new ProcessTopic(s).start();
            }
        } catch (SocketException e) {
            System.out.println("Socket: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO: " + e.getMessage());
        }
    }

    /**
     * Sending the heartbeat to event manager
     */
    public void startHeartBeat() {
        while (true) {
            communicator.sendHeartBeat();
            try {
                sleep(HEARTBEAT_RATE);
            } catch (InterruptedException ignored) {
            }
        }
    }
}

class ProcessEvent extends Thread {
    Socket socket;

    public ProcessEvent(Socket s) {
        this.socket = s;
    }

    public void run() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            int topics_count = objectInputStream.readInt();
            System.out.println("\n **** Notification received for subscribed topic ****");
            while (topics_count > 0) {
                Event event = (Event) objectInputStream.readObject();
                System.out.println(event);
                topics_count--;
            }
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

class ProcessTopic extends Thread {
    Socket socket;

    public ProcessTopic(Socket s) {
        this.socket = s;
    }

    public void run() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            int topics_count = objectInputStream.readInt();
            System.out.println("\n **** New Topic Available to subscribe or advertise  ****");
            while (topics_count > 0) {
                Topic topic = (Topic) objectInputStream.readObject();
                System.out.println(topic);
                topics_count--;
            }
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}