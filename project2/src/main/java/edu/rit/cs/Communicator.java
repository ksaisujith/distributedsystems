package edu.rit.cs;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Communicator class that handles communication between PubSub and EventManager
 */
public class Communicator {
    // Class variables required
    final int PORT;
    final String HOST;
    final int RMI_PORT;
    final int HEARTBEAT_PORT;
    private int ID;
    private DatagramSocket heartBeatSocket;
    private static DatagramPacket heartBeatpacket = null;
    private static Subscriber subscriber_stub;
    private static Publisher publisher_stub;

    /**
     * Communicator object to communicate with the event manager
     *
     * @param id       ID of Pub or Sub
     * @param port     PORT number used to connect to Event Manager
     * @param hostname HostName of the eventManager
     * @param rmi_port Port to connect RMI registry at Event Manager
     * @param hbport   Port to send heart beat
     */
    public Communicator(int id, int port, String hostname, int rmi_port, int hbport) {
        // Setting the class variables
        PORT = port;
        HOST = hostname;
        RMI_PORT = rmi_port;
        HEARTBEAT_PORT = hbport;
        this.ID = id;
        // Setting up connection with RMI
        try {
            heartBeatSocket = new DatagramSocket();
            String url = "rmi://" + HOST + ":" + RMI_PORT + "/";
            subscriber_stub = (Subscriber) Naming.lookup(url + "Subscriber");
            publisher_stub = (Publisher) Naming.lookup(url + "Publisher");
            byte[] buffer = ByteBuffer.allocate(4).putInt(ID).array();
            heartBeatpacket = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(HOST), HEARTBEAT_PORT);
        } catch (NotBoundException | IOException ex) {
            System.out.println(ex.getMessage());
        }

        // Checking the connection to event manager
        try (Socket ignored = new Socket(hostname, port)) {
        } catch (Exception e) {
            System.out.println("No connection formed. \nPlease check the hostname and port to connect to Event Manager and retry");
            System.exit(0);
        }
        // Validating the ID given
        validate_id();
    }

    /**
     * Connects to Event Manager and validates the given ID. If provided ID is not valid, then a new ID is assigned by
     * EventManager
     */
    public void validate_id() {
        int id = this.ID;
        try (Socket socket = new Socket(HOST, PORT)) {
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            // Sending the ID
            dos.writeInt(this.ID);
            // Receiving the same ID if valid else a new valid
            this.ID = dis.readInt();
        } catch (IOException e) {
            // Can't connect to event Manager
            System.exit(0);
        }

        // new ID is generated
        if (this.ID != id) {
            System.out.println(" Provided ID doesn't exists. \n New ID has been created.\n Your ID is:" + ID);
            try {
                byte[] buffer = ByteBuffer.allocate(4).putInt(ID).array();
                heartBeatpacket = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(HOST), HEARTBEAT_PORT);
            } catch (UnknownHostException ignored) {
            }
        } else {
            System.out.println("Connected to event manager");
        }
        // Starting a heartbeat Thread
        new PubSubMain(2).start();
    }

    /**
     * Gets all the available topics from Server
     *
     * @return List of all available topics at Event Manager
     * @throws RemoteException remote exception
     */
    public List<Topic> getAllTopics() throws RemoteException {
        return publisher_stub.listTopics();
    }

    /**
     * Subscribe a topic for notification by name of a topic
     *
     * @param TopicName Name of the topic to subscribe
     * @throws RemoteException Exception caused while connecting to RMI server
     */
    public void subscribeByName(String TopicName) throws RemoteException {
        if (subscriber_stub.subscribeByName(this.ID, TopicName))
            System.out.println("Successfully subscribed to topic");
        else
            System.out.println("Failed to subscribe to topic.\n" +
                    "Please check if topic Id exists");
    }

    /**
     * Subscribe a topic for notification by ID of a topic
     *
     * @param topicID ID of the topic to subscribe
     * @throws RemoteException Exception caused while connecting to RMI server
     */
    public void subscribeByID(int topicID) throws RemoteException {
        if (subscriber_stub.subscribe(this.ID, topicID))
            System.out.println("Successfully subscribed to topic");
        else
            System.out.println("Failed to subscribe to topic.\n" +
                    "Please check if topic Id exists");
    }

    /**
     * Subscribe a topic for notification by any key of a topic
     *
     * @param key Key to search for in available topics
     * @throws RemoteException Exception caused while connecting to RMI server
     */
    public void subscribeByKey(String key) throws RemoteException {
        if (subscriber_stub.subscribe(this.ID, key))
            System.out.println("Successfully subscribed to topic");
        else
            System.out.println("Failed to subscribe to topic.\n" +
                    "Please check if key exists in any topic");
    }

    /**
     * Unsubscribe a topic for notification by ID of a topic
     *
     * @param topicID ID of the topic to unsubscribe
     * @throws RemoteException Exception caused while connecting to RMI server
     */
    public void unsubscribeByID(int topicID) throws RemoteException {
        if (subscriber_stub.unsubscribe(this.ID, topicID))
            System.out.println("Successfully unsubscribed from the topic");
        else
            System.out.println("Failed to unsubscribe from topic.\n" +
                    "Please check if topic ID exists");
    }

    /**
     * Unsubscribe of all the subscribed topics
     *
     * @throws RemoteException Exception caused while connecting to RMI server
     */
    public void unsubscribeAll() throws RemoteException {
        if (subscriber_stub.unsubscribe(this.ID))
            System.out.println("Successfully unsubscribed from all topics");
        else
            System.out.println("Failed to unsubscribe from all topics.\n");
    }

    /**
     * Returns a list of topics that are subscribed
     *
     * @return Get list of all the topics that are subscribed
     * @throws RemoteException Exception caused while connecting to RMI server
     */
    public List<Topic> getSubscribedTopics() throws RemoteException {
        return subscriber_stub.listSubscribedTopics(this.ID);
    }

    /**
     * Publishes a new event to the topic
     *
     * @param newEvent New event to publish for a topic
     * @throws RemoteException Exception caused while connecting to RMI server
     */
    public void publish(Event newEvent) throws RemoteException {
        if (publisher_stub.publish(newEvent))
            System.out.println("Published Successfully");
        else
            System.out.println("Faced an error while publishing. \n" +
                    "Check if the Topic ID exists");
    }

    /**
     * Advertise a new topic
     * @param newTopic Topic to add
     * @throws RemoteException Exception caused while connecting to RMI server
     */
    public void advertise(Topic newTopic) throws RemoteException {
        if (publisher_stub.advertise(newTopic))
            System.out.println("Advertised new topic successfully");
        else
            System.out.println("Faced an error while advertising. \n" +
                    "Check if topic name already exists");
    }

    /**
     * Exits by notifying the event manager
     * @throws RemoteException Exception caused while connecting to RMI server
     */
    public void exit() throws RemoteException {
        if (subscriber_stub.offline(ID))
            System.out.println("Marked offline at event manager");
        else
            System.out.println("Failed to mark offline");
    }

    /**
     * Sends heartbeat to the event manager to let it know it is still connected to event manager
     */
    public void sendHeartBeat() {
        try {
            heartBeatSocket.send(heartBeatpacket);
        } catch (IOException ex) {
            System.out.println("Heartbeat Not sent");
            System.out.println(ex.getMessage());
        }
    }
}
