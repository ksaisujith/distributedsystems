package edu.rit.cs;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Subscriber extends Remote {

    /*
     * subscribe to a topic with matching keywords
     */
    public boolean subscribeByName(int subscriberId, String TopicName) throws RemoteException;

    /*
     * subscribe to a topic
     */
    public boolean subscribe(int subscriberId, int topic_id) throws RemoteException;

    /*
     * subscribe to a topic with matching keywords
     */
    public boolean subscribe(int subscriberId, String keyword) throws RemoteException;

    /*
     * unsubscribe from a topic
     */
    public boolean unsubscribe(int subscriberId, int topic_id) throws RemoteException;

    /*
     * unsubscribe to all subscribed topics
     */
    public boolean unsubscribe(int subscriberId) throws RemoteException;

    /*
     * show the list of topics current subscribed to
     */
    public List<Topic> listSubscribedTopics(int subscriberId) throws RemoteException;

    /*
     * Subscriber exiting. Markdown as offline
     */
    public boolean offline(int subscriberId) throws RemoteException;
}
