package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordCount_Seq_Improved {

    /**
     * Read and parse all reviews
     *
     * @param dataset_file Location of the file that is received
     * @return list of reviews
     */
    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        System.out.println("Reading the reviews");
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))) {
            // Skipping the header
            String reviewLine = br.readLine();
            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * Print the list of words and their counts
     *
     * @param wordcount Cound of words to write to a file
     */
    public static void create_result_file(Map<String, Integer> wordcount, String file) {
        String result_stmt;
        try (FileOutputStream out = new FileOutputStream(file)) {
            for (String word : wordcount.keySet()) {
                result_stmt = word + " : " + wordcount.get(word) + "\n";
                out.write(result_stmt.getBytes());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Emit 1 for every word and store this as a <key, value> pair
     * @param allReviews All reviews in the file
     * @return cound of words
     */
    public static List<KV<String, Integer>> map(List<AmazonFineFoodReview> allReviews) {
        List<KV<String, Integer>> kv_pairs = new ArrayList();

        for(AmazonFineFoodReview review : allReviews) {
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while(matcher.find())
                kv_pairs.add(new KV(matcher.group().toLowerCase(), 1));
        }
        return kv_pairs;
    }


    /**
     * count the frequency of each unique word
     * @param kv_pairs pair of words
     * @return a list of words with their count
     */
    public static Map<String, Integer> reduce(List<KV<String, Integer>> kv_pairs) {
        Map<String, Integer> results = new HashMap<>();

        for(KV<String, Integer> kv : kv_pairs) {
            if(!results.containsKey(kv.getKey())) {
                results.put(kv.getKey(), kv.getValue());
            } else {
                int init_value = results.get(kv.getKey());
                results.replace(kv.getKey(), init_value, init_value + kv.getValue());
            }
        }
        return results;
    }


    /**
     * Function that calls respective functions to count and return the couts
     *
     * @param reviews_file File in which words to be counted
     * @return a dictionary with the counts
     */
    public static Map<String, Integer> count_words(String reviews_file) {
        List<AmazonFineFoodReview> allReviews = read_reviews(reviews_file);

        System.out.println("Finished reading all reviews, now performing word count...");

        MyTimer myMapTimer = new MyTimer("map operation");
        myMapTimer.start_timer();
        List<KV<String, Integer>> kv_pairs = map(allReviews);
        myMapTimer.stop_timer();

        MyTimer myReduceTimer = new MyTimer("reduce operation");
        myReduceTimer.start_timer();
        Map<String, Integer> results = reduce(kv_pairs);
        myReduceTimer.stop_timer();
        myReduceTimer.print_elapsed_time();

        myMapTimer.print_elapsed_time();
        myReduceTimer.print_elapsed_time();
        System.out.println("Returning Results to client...");
        return results;
    }
}
