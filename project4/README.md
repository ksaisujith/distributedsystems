To build the RAFT network, below command is run in which Number of nodes is 5 as per this code.
This can also be scaled by changing the TOTAL_NODES value in Node.java and Communicator.java. 
```shell script
docker-compose -f docker-compose-project4.yml -p ds up --build -d --scale Node= <Number of nodes>
```
The above command creates image and hosts it on <Number of nodes> of containers. It also sets up 
the communicator through which client interacts with the network 

Below commands are used to connect the container and start the node in RAFT network with the ID provided. 
It automatically starts its timer and election phase to elect the leader
```shell script
docker exec -it ds_Node_<NodeID> /bin/bash
java -cp project4/target/project4-1.0-SNAPSHOT.jar edu.rit.cs.Node <Integer NodeID>
```

Below commands create a container and start a client. Onscreen instructions can be followed to interact
with the network. 

```shell script
docker run -it --network=ds_csci652network csci652:latest bash
java -cp project4/target/project4-1.0-SNAPSHOT.jar edu.rit.cs.Client
```