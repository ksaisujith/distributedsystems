package edu.rit.cs;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class Communicator extends UnicastRemoteObject implements Log, Remote {

    static final int RMI_PORT = 9012;
    static final int NODE_PORT = 9090;
    static final int TOTAL_NODES = 5, RETRY = 3;
    static final int TIMEOUT = 4000;
    static final int NODE_COMM_PORT = 9091;
    static final int RESPONSE_TIMEOUT = 20 * TOTAL_NODES * 1000;

    protected Communicator() throws RemoteException {
    }

    /**
     * Adds an entry to the log
     *
     * @param entry log_entry to add
     * @return true if successfully added
     */
    @Override
    public boolean addToLog(int entry) {
        int communication_type = 1;
        boolean success;
        InetAddress leader = this.getLeader(0);
        if (leader == null)
            return false;
        try (Socket socket = new Socket(leader, NODE_COMM_PORT)) {
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            socket.setSoTimeout(RESPONSE_TIMEOUT);

            if (in.readBoolean()) {
                // Writing Communication Type
                out.writeInt(communication_type);
                out.flush();

                // Sending the Entry
                out.writeInt(entry);
                out.flush();
            } else
                throw new IOException();

            success = in.readBoolean();
        } catch (IOException ex) {
            ex.printStackTrace();
            success = false;
        }
        return success;
    }

    /**
     * Gets the current log file
     *
     * @return log file as list
     */
    @Override
    public List<Integer> getLogFile() {
        int communication_type = 2;
        InetAddress leader = this.getLeader(0);
        System.out.println("LEADER IS: " + leader);
        if (leader == null)
            return null;
        try (Socket socket = new Socket(leader.getHostAddress(), NODE_COMM_PORT)) {
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            socket.setSoTimeout(RESPONSE_TIMEOUT);

            System.out.println("Waiting for server");
            if (in.readBoolean()) {
                System.out.println(" Server READY ");

                System.out.print("Sending Type");
                out.writeInt(communication_type);
                out.flush();
                System.out.println(" -- DONE");
            }

            // Waiting for response
            return (List<Integer>) in.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Retrieves an entry at the position
     *
     * @param index position
     * @return Integer value at index poistion in log
     */
    @Override
    public Integer getEntryAtIndex(int index) {
        int communication_type = 3;
        InetAddress leader = this.getLeader(0);
        System.out.println("LEADER IS: " + leader);
        if (leader == null)
            return null;
        try (Socket socket = new Socket(leader, NODE_COMM_PORT)) {
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            socket.setSoTimeout(RESPONSE_TIMEOUT);

            // Waiting if server ready
            if (in.readBoolean()) {
                // Write Communication type
                System.out.print("WRITING COMM TYPE");
                out.writeInt(communication_type);
                out.flush();
                System.out.println("-- DONE");

                // Writing the index
                out.writeInt(index);
                out.flush();
            } else
                throw new IOException();

            // Get the result
            return in.readInt();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Delete an entry in the log
     *
     * @param value Value to delete in the log
     * @return True if success
     */
    public boolean deleteEntry(int value) {
        int communication_type = 4;
        InetAddress leader = this.getLeader(0);
        System.out.println("LEADER IS: " + leader);
        try (Socket socket = new Socket(leader, NODE_COMM_PORT)) {
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            socket.setSoTimeout(RESPONSE_TIMEOUT);

            // Waiting if server ready
            if (in.readBoolean()) {
                // Write Communication type
                System.out.print("WRITING COMM TYPE");
                out.writeInt(communication_type);
                out.flush();
                System.out.println("-- DONE");

                out.writeInt(value);
                out.flush();
                return in.readBoolean();
            } else
                throw new IOException();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * Finds the leader at the point
     *
     * @param depth retry limit
     * @return InetAddress of the leader
     */
    private InetAddress getLeader(int depth) {
        if (depth > RETRY)
            return null;

        byte[] data = new byte[8];
        int packet_type = 5;
        // Adding my Node ID information
        for (int index = 0; index < 4; index++)
            data[index] = (byte) (99 >> (8 * index));

        for (int index = 4; index < 8; index++)
            data[index] = (byte) (packet_type >> (8 * index));


        try (DatagramSocket send_sokcet = new DatagramSocket();
             DatagramSocket rcv_socket = new DatagramSocket(NODE_PORT)) {
            rcv_socket.setSoTimeout(TIMEOUT);
            for (int node = 1; node <= TOTAL_NODES; node++) {
                DatagramPacket send_pckt = new DatagramPacket(data, data.length,
                        InetAddress.getByName("ds_Node_" + node), NODE_PORT);
                send_sokcet.send(send_pckt);
            }
            byte[] rcvd_data = new byte[128];
            DatagramPacket rcv_packet = new DatagramPacket(rcvd_data, rcvd_data.length);
            rcv_socket.receive(rcv_packet);

            ObjectInputStream oos = new ObjectInputStream(new ByteArrayInputStream(rcvd_data));
            return (InetAddress) oos.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return getLeader(depth + 1);
        }
    }

    /**
     * RMI service
     *
     * @throws RemoteException exception
     */
    public void startService() throws RemoteException {
        Registry registry;
        try {
            // Starting the RMI service
            registry = LocateRegistry.createRegistry(RMI_PORT);
            System.out.println("RMI registry created");
        } catch (RemoteException ex) {
            registry = LocateRegistry.getRegistry();
            System.out.println("RMI registry exists");
        }

        try {
            // Adding the objects to access
            Log log_entry = new Communicator();
            registry.rebind("communicate", log_entry);
            System.out.println("Registry Started");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Main process
     *
     * @param args command line arguemnts
     */
    public static void main(String[] args) {
        try {
            new Communicator().startService();
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Press any key to exit");
            in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
