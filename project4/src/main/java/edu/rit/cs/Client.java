package edu.rit.cs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.NotBoundException;

public class Client {
    static int RMI_PORT = 9012;
    static Log log;

    /**
     * Client object creation and connecting to communicator
     */
    Client() {
        try {
            String url = "rmi://" + InetAddress.getByName("ds_Communicator_1").getHostAddress() + ":"
                    + RMI_PORT + "/communicate";
            log = (Log) Naming.lookup(url);
        } catch (IOException | NotBoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Starts the clients process
     */
    public void begin() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            this.showOptions();
            try {
                int choice = Integer.parseInt(in.readLine());
                switch (choice) {
                    case 1:
                        System.out.println("Please enter the entry to add into the log");
                        if (log.addToLog(Integer.parseInt(in.readLine())))
                            System.out.println("SucCcessfully added");
                        else
                            System.out.println("Something went wrong while adding. Please try again");
                        break;
                    case 2:
                        System.out.println(log.getLogFile());
                        break;
                    case 3:
                        System.out.println("Enter the index");
                        System.out.println(log.getEntryAtIndex(Integer.parseInt(in.readLine())));
                        break;
                    case 4:
                        System.out.println("Enter the value to delete");
                        if (log.deleteEntry(Integer.parseInt(in.readLine())))
                            System.out.println("Deleted Successfully :)");
                        else
                            System.out.println("Something went wrong :(");
                        break;
                    case 5:
                        System.out.println("Bye Bye");
                        System.exit(1);
                    default:
                        System.out.println("Wrong input provided");
                }
            } catch (IOException | NumberFormatException ex) {
                System.out.println("Wrong input. Please try again");
            }

        }
    }

    /**
     * Displaying options
     */
    public void showOptions() {
        System.out.println("Please enter the index number \n" +
                "1. Add entry to log \n" +
                "2. Get current log \n" +
                "3. Get entry at particular index from the log \n" +
                "4. Delete an entry in the log\n" +
                "5. Exit");
    }

    /**
     * Starting the process
     *
     * @param args commandline arguments
     */
    public static void main(String[] args) {
        new Client().begin();
    }
}
