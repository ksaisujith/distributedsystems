package edu.rit.cs;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Node extends Thread {
    static final int LISTEN_PORT = 9090;
    static final int NODE_COMM_PORT = 9091;
    static final ReentrantLock LOCK = new ReentrantLock();
    static final Condition ELECTION_LOCK = LOCK.newCondition();
    static final ReentrantLock APPEND_ENTRIES = new ReentrantLock();
    static final Condition ENTRY_LOCK = APPEND_ENTRIES.newCondition();
    static boolean iAmLeader = false;
    static int TOTAL_NODES = 5;
    static final int MAJORITY_VOTES = (TOTAL_NODES >> 1) + 1;
    static final int HEARTBEAT_TIMEOUT = 300, RESPONSE_TIMEOUT = 1000;
    static int ELECTION_TIMEOUT_LOWER = 2000, ELECTION_TIMEOUT_UPPER = 4000;
    static int LEADER_ID = -1, TERM = 0;
    static List<Integer> log = new ArrayList<>();
    static Map<Integer, InetAddress> ALL_NODES;
    static int ID;
    static Set<Integer> rcvd_votes = new HashSet<>();
    static Set<Integer> rcvd_ack = new HashSet<>();
    byte[] packet_data;
    int threadType;
    Socket socket;
    boolean show_reset_time = false;

    /**
     * @param id NodeID
     */
    Node(int id) {
        ALL_NODES = new HashMap<>();
        ID = id;
        try {
            for (int node = 1; node <= TOTAL_NODES; node++)
                ALL_NODES.put(node, InetAddress.getByName("ds_Node_" + node));
        } catch (UnknownHostException ex) {
            System.out.println("Missing node info for at least one Node.\n" +
                    "Please try restarting network");
            System.exit(1);
        }

        // heartBeat listening Thread
        new Node(id, 1).start();

        // Election Thread
        new Node(id, 4).start();

        // Client communication Thread
        new Node(id, 5).start();
    }

    /**
     * Thread for different types
     * @param id   NodeID
     * @param type Type of thread
     *             1: Listen Incoming packets
     *             2: Sending HeartBeat Thread
     *             3: Packet processing
     *             4: Election timeout thread
     */
    Node(int id, int type) {
        ID = id;
        this.threadType = type;
    }

    /**
     * Incoming processing thread
     *
     * @param id   NodeID
     * @param type always 3. packet processing
     * @param data Received data that has to be processed
     */
    Node(int id, int type, byte[] data) {
        ID = id;
        this.threadType = type;
        this.packet_data = data;
    }

    /**
     * Thread that handles request from communicator
     *
     * @param sokt Incoming socket
     * @param type Thread type
     */
    Node(Socket sokt, int type) {
        this.socket = sokt;
        this.threadType = type;
    }

    /**
     * Main method
     *
     * @param args args[0] is the ID of the node
     */
    public static void main(String[] args) {
        try {
            new Node(Integer.parseInt(args[0])).begin();
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
            System.out.println("Wrong params provided \n" +
                    "USAGE: java -cp project4/target/project4-1.0-SNAPSHOT.jar edu.rit.cs.Node <INTEGER ID>");
            System.exit(1);
        }
    }

    /**
     * Starting point of the node
     */
    public void begin() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                System.out.println("Please enter the index of each opton \n" +
                        "1. Show Leader \n" +
                        "2. Show term \n" +
                        "3. Show Log \n" +
                        "4. exit");
                int choice = Integer.parseInt(in.readLine());
                switch (choice) {
                    case 1:
                        System.out.println(LEADER_ID);
                        break;
                    case 2:
                        System.out.println(TERM);
                        break;
                    case 3:
                        System.out.println(log);
                        break;
                    case 4:
                        System.exit(1);
                    default:
                        System.out.println("Invalid input given");
                }
            } catch (NumberFormatException | IOException ex) {
                System.out.println("Invalid input");
            }
        }
    }

    /**
     * Thread that receives all the incoming packets
     */
    @SuppressWarnings("InfiniteLoopStatement")
    private void listenIncomingMsg() {
        while (true) {
            byte[] data = new byte[16];
            DatagramPacket packet = new DatagramPacket(data, data.length);
            try (DatagramSocket socket = new DatagramSocket(LISTEN_PORT)) {
                socket.receive(packet);
                new Node(ID, 3, data).start();
            } catch (IOException ex) {
                System.out.println("Something went wrong while listening for the packet :( ");
            }
        }
    }

    /**
     * Processes the incoming packets based on its type
     * packet_type
     * 1: HeartBeat
     * 2: Requested for vote
     * 3: Voting for this term
     * 4: Append payload
     * 5: LEADER ID request
     * 6: Storing ACK for append entries
     * 7: Rollback append entries
     * 8: Delete a value in the log
     */
    private void processPacket() {
        int from = byteToInt(Arrays.copyOfRange(packet_data, 0, 4));
        int type = byteToInt(Arrays.copyOfRange(packet_data, 4, 8));
        int term = byteToInt(Arrays.copyOfRange(packet_data, 8, 12));

        // Reset election timeout
        switch (type) {
            case 1:
                if (term >= TERM) {
                    LOCK.lock();
                    ELECTION_LOCK.signalAll();
                    LOCK.unlock();
                    if (from != ID && term >= TERM) {
                        iAmLeader = false;
                    }
                    LEADER_ID = from;
                    TERM = term;
                }
                break;
            case 2:
                if (term >= TERM && from != ID) {
                    byte[] vote = intToByte(from);
                    TERM = term;
                    this.sendMsg(vote, from, 3);
                    LEADER_ID = from;
                    TERM++;
                }
                break;
            case 3:
                if (term == TERM && from != ID) {
                    rcvd_votes.add(from);
                    if (rcvd_votes.size() >= MAJORITY_VOTES)
                        iAmLeader = true;
                }
                break;
            case 4:
                if (from == LEADER_ID) {
                    int entry_value = byteToInt(Arrays.copyOfRange(packet_data, 12, 16));
                    if (log.size() == 0)
                        log.add(entry_value);
                    else if (log.get(log.size() - 1) != entry_value)
                        log.add(entry_value);
                    this.sendMsg("DONE".getBytes(), from, 6);
                } else {
                    System.out.println("DISCARDING UPDATE BECAUSE OF WRONG LEADER ID");
                }
                break;
            case 5:
                // Respond leader ID
                try (DatagramSocket send_socket = new DatagramSocket()) {
                    InetAddress local = ALL_NODES.getOrDefault(LEADER_ID, InetAddress.getLocalHost());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(baos);

                    oos.writeObject(local);
                    oos.flush();

                    // Object's byte array
                    byte[] bytes = baos.toByteArray();
                    DatagramPacket packet = new DatagramPacket(bytes, bytes.length,
                            InetAddress.getByName("ds_Communicator_1"), LISTEN_PORT);
                    send_socket.send(packet);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
            case 6:
                rcvd_ack.add(from);
                if (rcvd_ack.size() >= MAJORITY_VOTES) {
                    APPEND_ENTRIES.lock();
                    ENTRY_LOCK.signalAll();
                    APPEND_ENTRIES.unlock();
                }
            case 7:
                int entry_value = byteToInt(Arrays.copyOfRange(packet_data, 12, 16));
                if (log.size() > 0 && log.get(log.size() - 1) == entry_value) {
                    log.remove(log.size() - 1);
                }
                break;
            case 8:
                int del_value = byteToInt(Arrays.copyOfRange(packet_data, 12, 16));
                if (log.contains(del_value)) {
                    log.remove((Integer) del_value);
                }
                break;
            default:
                System.out.println("Unknown/Corrupt packet type. Discarding the packet");
        }
    }

    /**
     * Sends UDP packets to all the nodes participating in RAFT protocol
     *
     * @param payload     Payload in the packet
     * @param packet_type 1: HeartBeat
     *                    2: CallForVotes
     *                    3: MyVote
     *                    4: AppendEntry
     *                    Generated Packet Structure to send:
     *                    0______________4___________8______12_______
     *                    | SenderNodeID | packetType| TERM |payload|
     *                    ------------------------------------------
     */
    public void broadcast(byte[] payload, int packet_type) {
        byte[] data = new byte[16];

        // Adding my Node ID information
        for (int index = 0; index < 4; index++)
            data[index] = (byte) (ID >> (8 * index));

        for (int index = 4; index < 8; index++)
            data[index] = (byte) (packet_type >> (8 * index));

        for (int index = 8; index < 12; index++)
            data[index] = (byte) (TERM >> (8 * index));

        // Adding payload at the end of the packet
        System.arraycopy(payload, 0, data, 12, payload.length);

        try (DatagramSocket socket = new DatagramSocket()) {
            for (int nodeID : ALL_NODES.keySet()) {
                socket.send(new DatagramPacket(data, data.length, ALL_NODES.get(nodeID), LISTEN_PORT));
            }
        } catch (IOException exception) {
            System.out.println("Something went wrong while sending entries");
        }
    }

    /**
     * Sends a message
     *
     * @param payload          Body of the packet
     * @param communication_to Sending Response to
     */
    public void sendMsg(byte[] payload, int communication_to, int packet_type) {
        byte[] data = new byte[16];

        // Adding my Node ID information
        for (int index = 0; index < 4; index++)
            data[index] = (byte) (ID >> (8 * index));

        for (int index = 4; index < 8; index++)
            data[index] = (byte) (packet_type >> (8 * index));

        for (int index = 8; index < 12; index++)
            data[index] = (byte) (TERM >> (8 * index));

        // Adding payload at the end of the packet
        System.arraycopy(payload, 0, data, 12, payload.length);

        try (DatagramSocket socket = new DatagramSocket()) {
            socket.send(new DatagramPacket(data, data.length, ALL_NODES.get(communication_to), LISTEN_PORT));
        } catch (IOException exception) {
            System.out.println("Something went wrong while sending entries");
        }
    }

    /**
     * Leader sends the heartbeat
     */
    @SuppressWarnings({"BusyWait"})
    private void sendHeartBeat() {
        System.out.println("SENDING HEART BEAT");
        while (iAmLeader) {
            try {
                sleep(HEARTBEAT_TIMEOUT);
                this.broadcast("HEY".getBytes(), 1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Calls for election
     */
    public void callForElection() {
        System.out.println("Election started... Calling for Votes ");
        byte[] data = new byte[4];
        for (int index = 0; index < 4; index++)
            data[index] = (byte) (TERM >> (8 * index));
        broadcast(data, 2);
    }

    /**
     * Election timeout timer
     */
    @SuppressWarnings({"BusyWait"})
    public void election() {
        // Starts election timeout only if I am not a leader
        while (LEADER_ID != ID) {
            try {
                LOCK.lock();
                long randTime = ThreadLocalRandom.current().nextInt(ELECTION_TIMEOUT_LOWER, ELECTION_TIMEOUT_UPPER);
                if (show_reset_time)
                    System.out.println("TIMEOUT RESET TO: " + randTime);
                if (!ELECTION_LOCK.await(randTime, TimeUnit.MILLISECONDS)) {
                    // returns false if times out else true
                    this.callForElection();
                    sleep(RESPONSE_TIMEOUT);
                    System.out.println("RECEIVED VOTES " + rcvd_votes);
                    if (rcvd_votes.size() >= MAJORITY_VOTES) {
                        // Received majority of votes
                        LEADER_ID = ID;
                        iAmLeader = true;
                        new Node(ID, 2).start();
                        System.out.println("##### I AM THE LEADER NOW #######");
                    }
                    rcvd_votes = new HashSet<>();
                    TERM++;
                }
                LOCK.unlock();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        System.out.println("EXITING ELECTION PHASE");
    }

    /**
     * Handles the threads created
     */
    @Override
    public void run() {
        switch (threadType) {
            case 1:
                // Thread Listening for all the incoming messages
                listenIncomingMsg();
                break;
            case 2:
                // Thread sending HeartBeat
                sendHeartBeat();
                break;
            case 3:
                // Processing all the incoming packers
                processPacket();
                break;
            case 4:
                // Election timeout
                this.election();
                break;
            case 5:
                listenClientCommunication();
                break;
            case 6:
                this.handleSocket();
                break;
            default:
                System.out.println("Unknown Thread type");
        }
    }

    /**
     * Handles the request from client
     */
    public void handleSocket() {
        try (ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
             ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream())) {

            // Waiting for communication type
            System.out.print("Telling client I am ready");
            out.writeBoolean(true);
            out.flush();
            System.out.println(" -- DONE");

            int type = in.readInt();
            switch (type) {
                case 1:
                    // Waiting for entry
                    int newEntry = in.readInt();
                    boolean success = false;
                    APPEND_ENTRIES.lock();
                    int retry = 5;
                    while (!success && retry > 0 && rcvd_ack.size() < MAJORITY_VOTES) {
                        System.out.println("SENDING APPEND ENTRY REQUEST");
                        broadcast(intToByte(newEntry), 4);
                        try {
                            if (ENTRY_LOCK.await(RESPONSE_TIMEOUT * TOTAL_NODES, TimeUnit.MILLISECONDS)) {
                                // Commit changes
                                success = true;
                                System.out.println("COMMIT ENTRY");
                            } else {
                                System.out.println("Rolling back append request");
                                broadcast(intToByte(newEntry), 7);
                            }
                            System.out.println("Received ACKS From" + rcvd_ack);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        retry--;
                    }
                    rcvd_ack.clear();
                    out.writeBoolean(success);
                    out.flush();
                    APPEND_ENTRIES.unlock();
                    break;
                case 2:
                    System.out.print("SENDING LOG");
                    out.writeObject(log);
                    out.flush();
                    System.out.println(" -- DONE");
                    break;
                case 3:
                    System.out.print("SENDING EVENT");
                    int index = in.readInt();
                    out.writeInt(log.get(index));
                    out.flush();
                    System.out.println(" -- DONE");
                    break;
                case 4:
                    int delEntry = in.readInt();
                    broadcast(intToByte(delEntry), 8);
                    out.writeBoolean(true);
                default:
                    System.out.println("Wrong communication type");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Listens requests from client
     */
    @SuppressWarnings("InfiniteLoopStatement")
    public void listenClientCommunication() {
        try {
            ServerSocket server = new ServerSocket(NODE_COMM_PORT);
            while (true) {
                System.out.println("Listening");
                Socket skt = server.accept();
                new Node(skt, 6).start();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Converts an integer to Byte array
     *
     * @param integer Integer number
     * @return Converted byte array
     */
    private byte[] intToByte(int integer) {
        byte[] bytes = new byte[4];
        for (int index = 0; index < 4; index++)
            bytes[index] = (byte) (integer >> (8 * index));
        return bytes;
    }

    /**
     * Converts byte array to integer
     *
     * @param bytes byte array
     * @return Integer value
     */
    public int byteToInt(byte[] bytes) {
        return bytes[3] << 24 | (bytes[2] & 0xFF) << 16 | (bytes[1] & 0xFF) << 8 | (bytes[0] & 0xFF);
    }
}