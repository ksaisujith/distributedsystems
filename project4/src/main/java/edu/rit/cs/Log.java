package edu.rit.cs;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Log extends Remote {

    /**
     * Adds entry into the log
     *
     * @param entry log_entry to add
     */
    boolean addToLog(int entry) throws RemoteException;

    /**
     * Gets the entire log
     *
     * @return Entire Log
     */
    List<Integer> getLogFile() throws RemoteException;


    /**
     * Returns entry at index provided
     *
     * @param index position
     * @return entry value at index
     * @throws RemoteException exception
     */
    Integer getEntryAtIndex(int index) throws RemoteException;

    /**
     * Delete a value from the log
     *
     * @param key key value to delete
     * @return true if success
     * @throws RemoteException exception
     */
    boolean deleteEntry(int key) throws RemoteException;
}
